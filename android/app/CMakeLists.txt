cmake_minimum_required(VERSION 3.4.1)
set(GAME_DIR ${CMAKE_CURRENT_SOURCE_DIR}/android/app/)

include_directories(${GAME_DIR}/src/main/cpp)
set(GAME_SRC
    ${GAME_DIR}/src/main/cpp/main.cpp)