#include <vld.h>
#include "Game.h"

int main()
{
	Game game;
	game.Init("Pharaoh Must Die", 1280, 720, 254, 160);
	game.Run();
	return 0;
}