#include "Player.h"
#include "zengine/Input.h"
#include "zengine/Application.h"
#include "PatrolGuard.h"
#include "HidingSpot.h"
#include "Ladder.h"

Player::Player() :
	m_isHiding(false),
	m_hasWon(0),
	m_hasKey(false),
	m_direction(0),
	m_idleElapsed(0.0f),
	m_climbSpeed(0.0f),
	m_isClimbing(-2),
	m_isGrounded(true)
{
	m_name = "Player";
}
Player::~Player()
{
}

void Player::Init(float a_sneakSpeed, float a_walkSpeed, float a_climbSpeed)
{
	m_sneakSpeed = a_sneakSpeed;
	m_walkSpeed = a_walkSpeed;
	m_climbSpeed = a_climbSpeed;
	m_sprite.SetFlippedX(true);
	m_collider.SetSize(glm::vec2(6.0f, 25.0f));
	m_collider.SetPosition(glm::vec2(-3.0f, 0.0f));
	m_isHiding = false;
	m_hasWon = 0;
	m_isAmbushing = false;
	m_cameraPosition = m_transform.GetLocalPosition();
}
void Player::Update(float a_deltaTime)
{
	m_isAmbushing = false;
	m_direction = 0;

	if (Stab())
		return;

	if (Input::GetInstance()->MoveRight())
	{
		m_direction += 1;
	}
	if (Input::GetInstance()->MoveLeft())
	{
		m_direction -= 1;
	}

	float newYPosition = m_transform.GetLocalPosition().y;
	if (m_isClimbing != -2)
	{
		m_acceleration = 0.0f;
		if (m_isClimbing != 0)
		{
			newYPosition += m_climbSpeed * m_isClimbing * a_deltaTime;
		}
	}
	else 
	{
		m_acceleration -= 750.0f * a_deltaTime;
		if (m_acceleration < -300.0f)
			m_acceleration = -300.0f;
		 newYPosition += m_acceleration * a_deltaTime;
	}
	if (m_isGrounded || m_isClimbing != -2)
	{
		Move(a_deltaTime);
	}
	if (newYPosition <= 13.0f)
	{
		newYPosition = 13.0f;
		m_acceleration = 0.0f;
		m_isGrounded = true;
	}
	m_transform.SetPositionY(newYPosition);
	if(m_acceleration != 0.0f)
		m_isGrounded = false;

	Look(a_deltaTime);

	if (m_isHiding && Input::GetInstance()->UnHide())
	{
		m_collider.SetIsActive(true);
		m_isHiding = false;
		m_sprite.SetColour(glm::vec4(1.0f));
		m_isAmbushing = true;
	}
}

void Player::OnCollisionEnter(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "PatrolGuard")
	{
		Kill((PatrolGuard*)a_pawn);
	}
	else if (a_pawn->GetName() == "Shelf")
	{
		m_isGrounded = true;
		m_acceleration = 0.0f;
	}
}

void Player::OnCollisionStay(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "PatrolGuard")
	{
		Kill((PatrolGuard*)a_pawn);
	}
	else if (a_pawn->GetName() == "Shelf")
	{
		m_isGrounded = true;
		m_acceleration = 0.0f;
	}
}

void Player::OnCollisionExit(Pawn * a_pawn)
{

}

void Player::OnTriggerEnter(Pawn * a_pawn)
{
}
void Player::OnTriggerStay(Pawn * a_pawn)
{	
	if (a_pawn->GetName() == "HidingSpot")
	{
		Hide((HidingSpot*)a_pawn);
	}
	else if (a_pawn->GetName() == "Ladder")
	{
		Climb((Ladder*)a_pawn);
	}
}
void Player::OnTriggerExit(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Ladder")
	{
		m_isClimbing = -2;
	}
}

bool Player::Stab()
{
	if (m_sprite.GetCurrentAnimation() == 2)
	{
		if (!m_sprite.FinishedAnimation())
		{
			return true;
		}
		else
		{
			m_sprite.ChangeAnimation(0);
		}
	}
	return false;
}
void Player::Move(float a_deltaTime)
{
	if (m_hasWon != 0 || m_isHiding)
		return;
	if (m_direction == 1)
	{
		m_sprite.SetFlippedX(false);
		m_transform.Translate(glm::vec2(m_walkSpeed, 0.0f) * a_deltaTime);
		m_sprite.ChangeAnimation(1);
	}
	else if (m_direction == -1)
	{
		m_sprite.SetFlippedX(true);
		m_transform.Translate(glm::vec2(-m_walkSpeed, 0.0f) * a_deltaTime);
		m_sprite.ChangeAnimation(1);
	}
	else
	{
		m_sprite.ChangeAnimation(0);
	}
}
void Player::Look(float a_deltaTime)
{
	if (m_hasWon != 0)
		return;

	if (m_direction == 1)
	{
		m_cameraPosition = m_transform.GetLocalPosition() + glm::vec2(32.0f, 0.0f);
		m_idleElapsed = 0.0f;
	}
	else if (m_direction == -1)
	{
		m_cameraPosition = m_transform.GetLocalPosition() - glm::vec2(32.0f, 0.0f);
		m_idleElapsed = 0.0f;
	}
	else
	{
		m_idleElapsed += a_deltaTime;
		if(m_idleElapsed >= 1.25f)
			m_cameraPosition = m_transform.GetLocalPosition();
	}
}
void Player::Hide(HidingSpot* a_pawn)
{
	if (m_hasWon != 0)
		return;
	if (!m_isHiding && Input::GetInstance()->Hide())
	{
		m_collider.SetIsActive(false);
		m_isHiding = true;
		m_sprite.SetColour(glm::vec4(0.0f));
		m_transform.SetPositionX(a_pawn->m_transform.GetLocalPosition().x + a_pawn->m_transform.GetLocalSize().x * 0.5f);
	}
}
void Player::Kill(PatrolGuard * a_guard)
{
	bool dir;
	float behind = a_guard->GetBehind(dir);
	if (m_isAmbushing && a_guard->Kill())
	{
		m_transform.SetPositionX(behind);
		m_sprite.SetFlippedX(!dir);
		m_hasKey = true;
		m_sprite.ChangeAnimation(2);
	}
}
void Player::Climb(Ladder* a_ladder)
{
	m_isClimbing = 0;
	if (Input::GetInstance()->ClimbUp())
	{
		m_isClimbing += 1;
	}
	if (Input::GetInstance()->ClimbDown())
	{
		m_isClimbing -= 1;
	}
	 
	if (m_isClimbing == 1)
	{
		float topLadder = a_ladder->m_transform.GetLocalPosition().y + a_ladder->m_transform.GetLocalSize().y - 4.0f;
		if (m_transform.GetLocalPosition().y >= topLadder)
		{
			m_transform.SetPositionY(topLadder);
			m_isClimbing = 0;
		}
	}
}
void Player::SetHasWon(int a_value)
{
	m_hasWon = a_value;
	if (m_hasWon != 0)
	{
		m_isHiding = false;
		m_collider.SetIsActive(true);
		if(m_hasWon == 1)
			m_sprite.SetColour(glm::vec4(0.0f));
		else
			m_sprite.SetColour(glm::vec4(1.0f));
	}
}
void Player::ClampPosition(float a_min, const float a_max)
{
	float posX = m_transform.GetLocalPosition().x;
	if (posX < a_min)
	{
		posX = a_min;
		m_transform.SetPositionX(posX);
	}
	else if (posX > a_max)
	{
		posX = a_max;
		m_transform.SetPositionX(posX);
	}
}