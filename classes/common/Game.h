#pragma once
#include "zengine/Application.h"
#include "zengine/Pawn.h"

class Game : public Application
{
public:
	Game();
	~Game();

	void Load();
	void Update();
	void Render();
	void UnLoad();

	void LoadLevel(int a_major, int a_minor);
	void Fade(bool a_direction, float a_time);
	bool FinishedFading() { return m_fading == 0; }

private:
	Pawn m_fade;
	int m_fading;
	float m_fadeElapsed;
	float m_fadingTime;
};