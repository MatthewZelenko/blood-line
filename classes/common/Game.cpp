#include "Game.h"
#include "Menu/MainMenu.h"
#include "L1/L1_1.h"
#include "L1/L1_2.h"
#include "L1/L1Selector.h"
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#else
#include <GLEW/glew.h>
#endif

Game::Game() : 
	m_fading(0),
	m_fadeElapsed(1.0f)
{
	
}
Game::~Game()
{ 

}

void Game::Load()
{
	Shader* shader = ResourceManager::GetInstance()->CreateShader("normal", true);
	shader->Create();
	shader->AttachShaderFromFile("shaders/normal.vert", GL_VERTEX_SHADER);
	shader->AttachShaderFromFile("shaders/normal.frag", GL_FRAGMENT_SHADER);
	shader->LinkProgram();
	shader->Use();
	shader->BindLocation("position", 0);
	shader->BindLocation("texCoords", 1);

	Layer* layer = AddLayer();
	layer->SetStrength(0.0f);

	Texture* texture = ResourceManager::GetInstance()->CreateTexture("Fade", true);
	texture->Load("textures/Fade.png");
	ChangeScene(new MainMenu());
	m_fade.InitPawn(shader, texture, glm::vec2(), glm::vec2(GetGameWidth(), GetGameHeight()));
	m_fade.m_sprite.SetColour(glm::vec4(1.0f, 1.0f, 1.0f, 0.0f));
	layer->AddPawn(&m_fade);
	
	texture = ResourceManager::GetInstance()->CreateTexture("Settings", true);
	texture->Load("textures/Settings.png");

	texture = ResourceManager::GetInstance()->CreateTexture("Square", true);
	texture->Load("textures/Square.png");
}
void Game::Update()
{
	if (m_fading == 1)
	{
		m_fadeElapsed += GetDeltaTime();
		if (m_fadeElapsed >= m_fadingTime)
		{
			m_fadeElapsed = m_fadingTime;
			m_fading = 0;
		}
		m_fade.m_sprite.SetColour(glm::vec4(1.0f, 1.0f, 1.0f, m_fadeElapsed / m_fadingTime));
	}
	else if(m_fading == -1)
	{
		m_fadeElapsed -= GetDeltaTime();
		if (m_fadeElapsed <= 0.0f)
		{
			m_fadeElapsed = 0.0f;
			m_fading = 0;
		}
		m_fade.m_sprite.SetColour(glm::vec4(1.0f, 1.0f, 1.0f, m_fadeElapsed / m_fadingTime));
	}
}
void Game::Render()
{

}
void Game::UnLoad()
{

}

void Game::LoadLevel(int a_major, int a_minor)
{
	switch (a_major)
	{
	case 0:
		switch (a_minor)
		{
		case 0:
			Quit();
			return;
		case 1:
			ChangeScene(new MainMenu());
			return;
		}
		return;
	case 1:
	{
		switch (a_minor)
		{
		case 0:
			ChangeScene(new L1Selector());
			return;
		case 1:
			ChangeScene(new L1_1());
			return;
		case 2:
			ChangeScene(new L1_2());
			return;
		case 3:
			return;
		case 4:
			return;
		case 5:
			return;
		case 6:
			return;
		case 7:
			return;
		case 8:
			return;
		case 9:
			return;
		case 10:
			return;
		case 11:
			return;
		case 12:
			return;
		default:
			break;
		}
		return;
	}
	default:
		break;
	}
}

void Game::Fade(bool a_direction, float a_time)
{
	m_fadingTime = a_time;
	if (a_direction)
	{
		m_fading = 1;
		m_fadeElapsed = 0.0f;
	}
	else
	{
		m_fading = -1;
		m_fadeElapsed = a_time;
	}
	m_fade.m_sprite.SetColour(glm::vec4(1.0f, 1.0f, 1.0f, m_fadeElapsed / m_fadingTime));
}