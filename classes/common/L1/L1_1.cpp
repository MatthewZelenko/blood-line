#include "L1_1.h"
#include "zengine/Animation.h"
#include "zengine/Input.h"
#include "zengine/CollisionManager.h"
#include "Game.h"

L1_1::L1_1()
{
}
L1_1::~L1_1()
{
}

void L1_1::Load()
{
	Shader* shader = ResourceManager::GetInstance()->GetShader("normal");
	shader->Use();
	m_camera.AttachShader(shader);

	//SKYLAYER
	Layer* layer = AddLayer();
	layer->SetStrength(0.0f);
	Texture* texture = ResourceManager::GetInstance()->CreateTexture("Sky");
	texture->Load("textures/Levels/L1/1/Sky.png");
	m_sky.InitPawn(shader, texture);
	layer->AddPawn(&m_sky);

	//BGLAYER
	layer = AddLayer();
	layer->SetStrength(1.0f);

	//BG
	texture = ResourceManager::GetInstance()->CreateTexture("BG");
	texture->Load("textures/Levels/L1/1/Bg.png");
	m_bg.InitPawn(shader, texture);
	layer->AddPawn(&m_bg);

	//DOOR
	texture = ResourceManager::GetInstance()->CreateTexture("Door");
	texture->Load("textures/Levels/L1/Door/Door.png");
	m_door.InitPawn(shader, texture, glm::vec2(547.0f, 13.0f), glm::vec2(111.0f, 63.0f));
	//TODO: CHANGE TO 1, 2
	m_door.Init(1, 0);
	m_door.m_collider.SetPosition(glm::vec2(40.0f, 0.0f));
	m_door.m_collider.SetSize(glm::vec2(33.0f, 50.0f));
	CollisionManager::GetInstance()->Register(&m_door);
	layer->AddPawn(&m_door);

	Animation* anim = ResourceManager::GetInstance()->CreateAnimation("Door Idle");
	anim->AddFrame(glm::vec4(0.0f, 0.0f, 111.0f, 63.0f));
	m_door.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("Door Open");
	anim->m_frameDelay = 0.1f;
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(111.0f, 0.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(222.0f, 0.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(333.0f, 0.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(444.0f, 0.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(0.0f, 63.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(111.0f, 63.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(222.0f, 63.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(333.0f, 63.0f, 111.0f, 63.0f));
	anim->AddFrame(glm::vec4(444.0f, 63.0f, 111.0f, 63.0f));
	m_door.m_sprite.AddAnimation(anim);

	//Guide
	texture = ResourceManager::GetInstance()->CreateTexture("Guide");
	texture->Load("textures/Guide.png");
	m_guide.InitPawn(shader, texture);
	//layer->AddPawn(&m_guide);

	//CLOSET
	texture = ResourceManager::GetInstance()->CreateTexture("Closet");
	texture->Load("textures/Levels/L1/Closet.png");
	m_closet.InitPawn(shader, texture, glm::vec2(318.0f, 13.0f));
	m_closet.m_sprite.FramesChangeSize(true);

	anim = ResourceManager::GetInstance()->CreateAnimation("Closet Open");
	anim->AddFrame(glm::vec4(34.0f, 0.0f, 34.0f, 65.0f));
	m_closet.m_sprite.AddAnimation(anim);
	anim = ResourceManager::GetInstance()->CreateAnimation("Closet Closed");
	anim->AddFrame(glm::vec4(0.0f, 0.0f, 34.0f, 65.0f));
	m_closet.m_sprite.AddAnimation(anim);
	anim = ResourceManager::GetInstance()->CreateAnimation("Closet Glow");
	anim->AddFrame(glm::vec4(68.0f, 0.0f, 34.0f, 65.0f));
	m_closet.m_sprite.AddAnimation(anim);
	//m_closet.Init();

	CollisionManager::GetInstance()->Register(&m_closet);
	layer->AddPawn(&m_closet);

	//PLAYER
	texture = ResourceManager::GetInstance()->CreateTexture("Player");
	texture->Load("textures/Levels/L1/Player/Player.png");
	m_player.InitPawn(shader, texture, glm::vec2(40.0f, 13.0f), glm::vec2(20.0f, 25.0f));
	m_player.m_sprite.SetSource(glm::vec4(0.0f, 25.0f, 20.0f, 25.0f));
	m_player.Init(30.0f, 80.0f, 20.0f);
	m_player.m_sprite.SetFlippedX(false);
	CollisionManager::GetInstance()->Register(&m_player);

	anim = ResourceManager::GetInstance()->CreateAnimation("Player Idle");
	anim->m_frameDelay = 0.0f;
	anim->AddFrame(glm::vec4(0, 0.0f, 20.0f, 25), glm::vec2(6.0f, 0.0f));
	m_player.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("Player Walk");
	anim->m_frameDelay = 0.075f;
	anim->AddFrame(glm::vec4(0, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(20, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(40, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(60, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(80, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(100, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(120, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(140, 25.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	m_player.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("Player Stab");
	anim->m_frameDelay = 0.025f;
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(0, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(20, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(40, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(60, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(80, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(100, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	anim->AddFrame(glm::vec4(120, 50.0f, 20, 25.0f), glm::vec2(6.0f, 0.0f));
	m_player.m_sprite.AddAnimation(anim);
	layer->AddPawn(&m_player);

	//GUARD
	texture = ResourceManager::GetInstance()->CreateTexture("Guard");
	texture->Load("textures/Levels/L1/Guard/Guard.png");
	m_guard.InitPawn(shader, texture, glm::vec2(), glm::vec2(33.0f, 36.0f));
	m_guard.m_major = 1;
	m_guard.m_minor = 0;
	m_guard.m_sprite.SetFlippedX(false);
	m_guard.m_sprite.FramesChangeSize(true);
	m_guard.m_sprite.FramesChangeColliderSize(true);
	m_guard.GiveKey();
	CollisionManager::GetInstance()->Register(&m_guard);
	layer->AddPawn(&m_guard);

	anim = ResourceManager::GetInstance()->CreateAnimation("Guard Idle");
	anim->m_frameDelay = 0.0f;
	anim->AddFrame(glm::vec4(0.0f, 0.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	m_guard.m_sprite.AddAnimation(anim);

	//WALK
	anim = ResourceManager::GetInstance()->CreateAnimation("Guard Walk");
	anim->m_frameDelay = 0.1f;
	anim->AddFrame(glm::vec4(33.0f, 0.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(66.0f, 0.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(99.0f, 0.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(132.0f, 0.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(0.0f, 36.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(33.0f, 36.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(66.0f, 36.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(99.0f, 36.0f, 17.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	m_guard.m_sprite.AddAnimation(anim);

	//DEATH
	anim = ResourceManager::GetInstance()->CreateAnimation("Guard Death");
	anim->m_frameDelay = 0.075f;
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(132.0f, 36.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(0.0f, 72.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(33.0f, 72.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(66.0f, 72.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(99.0f, 72.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(132.0f, 72.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	anim->AddFrame(glm::vec4(0.0f, 108.0f, 33.0f, 36.0f), glm::vec2(4.0f, 0.0f));
	m_guard.m_sprite.AddAnimation(anim);

	m_guard.Init(30.0f, &m_player, 248.0f, 426.0f);

	//FGLAYER
	layer = AddLayer();
	layer->SetStrength(1.5f);
	texture = ResourceManager::GetInstance()->CreateTexture("FG");
	texture->Load("textures/Levels/L1/1/FG.png");
	m_fg.InitPawn(shader, texture);
	layer->AddPawn(&m_fg);

	layer = AddLayer();
	layer->SetStrength(0.0f);
	texture = ResourceManager::GetInstance()->GetTexture("Settings");
	m_backButton.InitPawn(shader, texture, glm::vec2(m_app->GetScreenLeft() + 4.0f, m_app->GetScreenTop() - 11 - 4.0f));
	m_backButton.m_major = 1;
	m_backButton.m_minor = 0;
	layer->AddPawn(&m_backButton);
	((Game*)m_app)->Fade(false, 1.0f);
}
void L1_1::Update(float a_deltaTime)
{
	if (m_player.HasWon() != 0)
	{
		if (((Game*)m_app)->FinishedFading())
		{
			if (m_player.HasWon() == -1)
			{
				((Game*)m_app)->LoadLevel(1, 0);
			}
			else
			{
				((Game*)m_app)->LoadLevel(1, 0);
			}
		}
		return;
	}
	m_player.ClampPosition(32.0f, 730.0f);
	m_camera.MoveTowards(m_player.GetCameraPosition() + glm::vec2(-m_app->GetGameWidth() * 0.5f, -m_app->GetGameHeight() * 0.5f), 2.0f * a_deltaTime);
	m_camera.ClampPosition(glm::vec2(0.0f, 0.0f), m_bg.m_transform.GetSize() - glm::vec2(m_app->GetGameWidth(), m_app->GetGameHeight()));
}
void L1_1::FixedUpdate(float a_fixedTime)
{
}
void L1_1::Render(float a_deltaTime)
{
	//CollisionManager::GetInstance()->Render();
}
void L1_1::UnLoad()
{
}