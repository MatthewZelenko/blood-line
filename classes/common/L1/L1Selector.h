#pragma once
#include "zengine/Scene.h"
#include "zengine/Pawn.h"
#include "Menu/LevelButton.h"

class L1Selector : public Scene
{
public:
	L1Selector();
	~L1Selector();

	void Load();
	void Update(float a_deltaTime);
	void FixedUpdate(float a_fixedTime);
	void Render(float a_deltaTime);
	void UnLoad();

	Pawn m_bg, m_guide;
	std::vector<LevelButton> m_buttons;
	LevelButton m_backButton;
};