#pragma once
#include "zengine/Scene.h"
#include "zengine/Pawn.h"
#include "Player.h"
#include "Door.h"
#include "Menu/LevelButton.h"
#include "PatrolGuard.h"
#include "Ladder.h"

class L1_2 : public Scene
{
public:
	L1_2();
	~L1_2();
	
	void Load();
	void Update(float a_deltaTime);
	void FixedUpdate(float a_fixedTime);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	//PatrolGuard m_guard;
	//Door m_door;
	Ladder m_ladder;
	Pawn m_bg, m_sky, m_shelf;
	LevelButton m_backButton;
	Player m_player;
};