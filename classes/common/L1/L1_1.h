#pragma once
#include "zengine/Scene.h"
#include "zengine/Pawn.h"
#include "Player.h"
#include "HidingSpot.h"
#include "Door.h"
#include "Menu\LevelButton.h"
#include "PatrolGuard.h"

class L1_1 : public Scene
{
public:
	L1_1();
	~L1_1();
	
	void Load();
	void Update(float a_deltaTime);
	void FixedUpdate(float a_fixedTime);
	void Render(float a_deltaTime);
	void UnLoad();

private:
	PatrolGuard m_guard;
	Door m_door;
	Pawn m_guide, m_bg, m_fg, m_sky;
	LevelButton m_backButton;
	Player m_player;
	HidingSpot m_closet;
};