#include "L1Selector.h"
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#else
#include <GLEW/glew.h>
#endif
#include "zengine/Log.h"
#include "Game.h"

L1Selector::L1Selector()
{
}
L1Selector::~L1Selector()
{
}

void L1Selector::Load()
{
	Shader* shader = ResourceManager::GetInstance()->GetShader("normal");
	shader->Use();
	m_camera.AttachShader(shader);

	Layer* layer = AddLayer();
	layer->SetStrength(1.0f);

	Texture* texture = ResourceManager::GetInstance()->CreateTexture("BG");
	texture->Load("textures/Menu/L1/BG.png");
	m_bg.InitPawn(shader, texture);
	layer->AddPawn(&m_bg);

	texture = ResourceManager::GetInstance()->CreateTexture("Buttons");
	texture->Load("textures/Menu/L1/Buttons.png");

	m_buttons.resize(12);
	for (int y = 0; y < 3; ++y)
	{
		for (int x = 0; x < 4; ++x)
		{
			LevelButton* btn = &m_buttons[(y * 4) + x];
			btn->InitPawn(shader, texture, glm::vec2(x * 22.0f, (2 - y) * 14.0f) + glm::vec2(84.0f, 60.0f), glm::vec2(21.0f, 13.0f));
			btn->m_major = 1;
			btn->m_minor = (y* 4) + x + 1;

			std::string name = "btn_" + Log::IntToString(((y * 4) + x)) + "_up";
			Animation* anim = ResourceManager::GetInstance()->CreateAnimation(name);
			anim->m_loops = 1;
			anim->AddFrame(glm::vec4(x * 21.0f * 2.0f, y * 13.0f, 21.0f, 13.0f), glm::vec2(1.0f,0.0f));
			btn->m_sprite.AddAnimation(anim);

			name = "btn_" + Log::IntToString(((y * 4) + x)) + "_down";
			anim = ResourceManager::GetInstance()->CreateAnimation(name);
			anim->m_loops = 1;
			anim->AddFrame(glm::vec4((x * 21.0f * 2.0f) + 21.0f, y * 13.0f, 21.0f, 13.0f), glm::vec2(1.0f, 0.0f));
			btn->m_sprite.AddAnimation(anim);

			layer->AddPawn(&m_buttons[(y * 4) + x]);
		}
	}

	//BACK BUTTON
	texture = ResourceManager::GetInstance()->CreateTexture("BackButton");
	texture->Load("textures/Menu/L1/Back Button.png");

	m_backButton.InitPawn(shader, texture, glm::vec2(52.0f, 18.0f), glm::vec2(21.0f, 13.0f));
	m_backButton.m_major = 0;
	m_backButton.m_minor = 1;
	Animation* anim = ResourceManager::GetInstance()->CreateAnimation("BackButton_Up");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(0.0f, 0.0f, 21.0f, 13.0f));
	m_backButton.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("BackButton_Down");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(21.0f, 0.0f, 21.0f, 13.0f));
	m_backButton.m_sprite.AddAnimation(anim);

	layer->AddPawn(&m_backButton);

	texture = ResourceManager::GetInstance()->CreateTexture("Guide");
	texture->Load("textures/Guide.png");
	m_guide.InitPawn(shader, texture);
	//layer->AddPawn(&m_guide);
	((Game*)m_app)->Fade(false, 0.0f);
}
void L1Selector::Update(float a_deltaTime)
{

}
void L1Selector::FixedUpdate(float a_fixedTime)
{

}
void L1Selector::Render(float a_deltaTime)
{

}
void L1Selector::UnLoad()
{
}