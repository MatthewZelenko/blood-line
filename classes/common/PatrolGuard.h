#pragma once
#include "zengine/Pawn.h"
#include "Player.h"

enum State
{
	IDLE, 
	PATROL,
	CHASE
};

class PatrolGuard :	public Pawn
{
public:
	PatrolGuard();
	~PatrolGuard();

	void Init(float a_speed, Player* a_player, float a_startPosition, float a_endPosition);

	void Update(float a_deltaTime);
	void GiveKey();
	bool Kill();
	float GetBehind(bool& a_direction);

	int m_major, m_minor;
private:
	bool m_isDead;
	Player* m_player;
	State m_state;
	float m_elapsed;
	float m_startPosition, m_endPosition;
	bool m_direction;
	float m_speed;
	bool m_hasKey;
};