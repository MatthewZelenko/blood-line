#include "Door.h"
#include "zengine/Input.h"
#include "Player.h"
#include "Game.h"
#include "zengine/Layer.h"

Door::Door()
{
	m_name = "Door";
	SetStatic(true);
	m_collider.SetIsTrigger(true);
}
Door::~Door()
{
}

void Door::Init(int a_majorLevel, int a_minorLevel)
{
	m_majorLevel = a_majorLevel;
	m_minorLevel = a_minorLevel;
}

void Door::OnTriggerEnter(Pawn * a_pawn)
{
	if (a_pawn->GetName() != "Player")
		return;
	if(((Player*)a_pawn)->HasKey())
		m_sprite.ChangeAnimation(1);
}
void Door::OnTriggerStay(Pawn * a_pawn)
{
	if (a_pawn->GetName() != "Player")
		return;

	if (((Player*)a_pawn)->HasWon() == 0 && ((Player*)a_pawn)->HasKey() && Input::GetInstance()->Hide() && m_sprite.GetCurrentAnimation() == 1 && m_sprite.FinishedAnimation() == 1)
	{
		((Player*)a_pawn)->SetHasWon(1);
		((Game*)GetLayer()->m_app)->Fade(true, 1.0f);
	}
}