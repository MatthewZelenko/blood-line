#include "Pawn.h"
#include "zengine/Log.h"

Pawn::Pawn():
	m_layerIndex(0),
	m_layer(nullptr)
{
	m_transform.SetParent(this);
	m_sprite.SetParent(this);
	m_collider.SetParent(this);
	m_name = "Pawn";
}
Pawn::~Pawn()
{
}

void Pawn::InitPawn(Shader * a_shader, Texture * a_texture, const glm::vec2& a_position, const glm::vec2& a_size)
{
	m_transform.SetPosition(a_position);
	glm::vec2 size = a_size;
	if (size.x == 0 || size.y == 0)
	{
		size.x = a_texture->GetWidth();
		size.y = a_texture->GetHeight();
	}
	m_transform.SetSize(size);
	m_collider.SetSize(size);

	m_sprite.SetShader(a_shader);
	m_sprite.SetTexture(a_texture);
}
void Pawn::Load()
{
}
void Pawn::Update(float a_deltaTime)
{
}
void Pawn::FixedUpdate(float a_fixedTime)
{
}
void Pawn::Render()
{

}

void Pawn::OnCollisionEnter(Pawn * a_pawn)
{
}
void Pawn::OnCollisionStay(Pawn * a_pawn)
{

}
void Pawn::OnCollisionExit(Pawn * a_pawn)
{
}

void Pawn::OnTriggerEnter(Pawn * a_pawn)
{
}

void Pawn::OnTriggerStay(Pawn * a_pawn)
{
}

void Pawn::OnTriggerExit(Pawn * a_pawn)
{
}

void Pawn::SetLayer(Layer* a_layer, int a_index)
{
	m_layer = a_layer;
	m_layerIndex = a_index;
}