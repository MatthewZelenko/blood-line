#pragma once
#include <GLM/glm.hpp>
#include <Windows.h>

enum KEYSTATE {
	PRESSED,
	HELD
};

class Application;
class Input
{
public:
	Input();
	~Input();

	static Input* GetInstance();
	static void DestroyInstance();

	void Update();
	
	bool Hide();
	bool UnHide();
	bool ClimbUp();
	bool ClimbDown();
	bool MoveLeft();
	bool MoveRight();
	bool Click(glm::vec2& a_point);
	bool Release(glm::vec2& a_point);

	void SetKey(WPARAM a_key, bool a_isDown);
	void SetClicked(bool a_value);
	void SetClickPosition(const glm::vec2& a_position);
	void SetReleased(bool a_value);

private:
	friend class Application;
	static Input* m_instance;

	Application* m_app;
	bool m_oldKeyState[256];
	bool m_keyState[256];
	bool m_released;
	bool m_clicked;
	glm::vec2 m_clickPosition;
};