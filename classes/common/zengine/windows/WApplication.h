#pragma once
#include <Windows.h>
#include <map>
#include <vector>
#include <string>
#include <zengine/Layer.h>

class Scene;
class Application
{
public:
	Application();
	~Application();

	void Init(char * a_title, int a_displayWidth, int a_displayHeight, int a_gameWidth, int a_gameHeight);

	void Run();

	virtual void Load();
	virtual void Update();
	virtual void FixedUpdate();
	virtual void Render();
	virtual void UnLoad();

	//SCENE
	void ChangeScene(Scene* a_scene);

	void Quit();

	//MISC
	int GetDisplayWidth() { return m_displayWidth; }
	int GetDisplayHeight() { return m_displayHeight; }
	int GetGameWidth() { return m_gameWidth; }
	int GetGameHeight() { return m_gameHeight; }
	float GetScreenLeft() { return m_xGameExt; }
	float GetScreenRight() { return m_gameWidth - m_xGameExt; }
	float GetScreenBottom() { return m_yGameExt; }
	float GetScreenTop() { return (m_gameHeight - m_yGameExt); }
	float GetDeltaTime() { return m_deltaTime; }
	float GetElapsedTime() { return m_elapsedTime; }
	float GetTimeStep() { return m_timeStep; }

	float m_xDisplayExt, m_yDisplayExt;
	float m_xGameExt, m_yGameExt;
	
	Layer* AddLayer();
	Layer* GetLayer(int a_index);
protected:


private:
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void SystemInit();
	void SystemUpdate();
	void SystemDestroy();
	int CreateDisplay();
	void DestroyDisplay();
	bool ShouldClose();
	LARGE_INTEGER GetTime();
	void UpdateTime();

	char* m_title;
	HWND m_wnd;
	HDC m_hdc;
	HGLRC m_rc;
	MSG m_msg;
	LARGE_INTEGER m_systemTime;

	//MISC
	float m_deltaTime, m_elapsedTime;
	float m_elapsedTimeStep, m_timeStep;
	int m_displayWidth, m_displayHeight;
	int m_gameWidth, m_gameHeight;

	//SCENE	
	void SceneLoad();
	void SceneUpdate();
	void SceneFixedUpdate();
	void SceneRender();
	void SceneUnLoad();

	Scene* m_currentScene;
	Scene* m_nextScene;

	void LoadLayers();
	void UpdateLayers();
	void FixedLayers();
	void RenderLayers();
	std::vector<Layer*> m_layers;
	Camera m_camera;
};