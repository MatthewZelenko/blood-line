#define GLEW_STATIC
#include "Zengine/windows/WApplication.h"
#include <GLEW/glew.h>
#include <time.h>
#include <windowsx.h>
#include "zengine/windows/Input.h"
#include "zengine/Log.h"
#include "zengine/ResourceManager.h"
#include "zengine/Renderer.h"
#include "zengine/Scene.h"
#include "zengine/CollisionManager.h"

Application::Application() :
	m_title(nullptr),
	m_elapsedTime(0.0f),
	m_deltaTime(0.0f),
	m_displayWidth(0.0f),
	m_displayHeight(0.0f),
	m_gameWidth(0.0f),
	m_gameHeight(0.0f),
	m_elapsedTimeStep(0.0f),
	m_timeStep(0.01f),
	m_currentScene(nullptr),
	m_nextScene(nullptr),
	m_xDisplayExt(0.0f),
	m_yDisplayExt(0.0f),
	m_xGameExt(0.0f),
	m_yGameExt(0.0f)
{
}
Application::~Application()
{

}

void Application::Init(char * a_title, int a_displayWidth, int a_displayHeight, int a_gameWidth, int a_gameHeight)
{
	m_title = a_title;
	m_displayWidth = a_displayWidth;
	m_displayHeight = a_displayHeight;
	m_gameWidth = a_gameWidth;
	m_gameHeight = a_gameHeight;
}
void Application::Run()
{
	if (!CreateDisplay())
		return;
	SystemInit();
	Load();
	LoadLayers();
	while (ShouldClose())
	{
		SystemUpdate();
		SceneLoad();
		SceneUpdate();
		Update();
		UpdateLayers();
		SceneFixedUpdate();
		if (!m_nextScene)
		{
			glClear(GL_COLOR_BUFFER_BIT);
			SceneRender();
			Render();
			RenderLayers();
			SwapBuffers(m_hdc);
		}
		Input::GetInstance()->Update();
	}
	SceneUnLoad();
	UnLoad();
	DestroyDisplay();
	SystemDestroy();
}
void Application::Load()
{
}
void Application::Update()
{
}
void Application::FixedUpdate()
{
}
void Application::Render()
{
}
void Application::UnLoad()
{
}

LARGE_INTEGER Application::GetTime()
{
	LARGE_INTEGER time;
	QueryPerformanceCounter(&time);
	return time;
}
void Application::UpdateTime()
{
	LARGE_INTEGER newTime = GetTime();
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);

	m_deltaTime = (double)((newTime.QuadPart - m_systemTime.QuadPart) * 1000.0 / freq.QuadPart) * 0.001;
	m_systemTime = newTime;
	m_elapsedTime += m_deltaTime;
	m_elapsedTimeStep += m_deltaTime;
}
int Application::CreateDisplay()
{
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT windowRect;
	windowRect.left = 0;
	windowRect.top = 0;
	windowRect.right = m_displayWidth;
	windowRect.bottom = m_displayHeight;

	//Register Window Class
	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = Application::WindowProc;
	wc.hInstance = GetModuleHandle(NULL);
	wc.lpszClassName = "OpenGLWindow";
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, "Could Not Register Class", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX;

	AdjustWindowRectEx(&windowRect, dwStyle, FALSE, dwExStyle);

	m_wnd = CreateWindowEx(dwExStyle, "OpenGLWindow", m_title, dwStyle, 0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, NULL, NULL, GetModuleHandle(NULL), NULL);

	//Create Window
	RECT clientRect;
	GetClientRect(m_wnd, &clientRect);
	windowRect.left += windowRect.left - clientRect.left;
	windowRect.right += windowRect.right - clientRect.right;
	windowRect.top += windowRect.top - clientRect.top;
	windowRect.bottom += windowRect.bottom - clientRect.bottom;
	MoveWindow(m_wnd, 0, 0, windowRect.right - windowRect.left, windowRect.bottom - windowRect.top, FALSE);

	if (!m_wnd)
	{
		MessageBox(NULL, "Could Not Create Window", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	//Create GL Context
	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory(&pfd, 0);
	pfd.cDepthBits = 32;
	pfd.cColorBits = 24;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.nVersion = 1;
	pfd.iPixelType = PFD_TYPE_RGBA;

	m_hdc = GetDC(m_wnd);
	if (!m_hdc)
	{
		MessageBox(NULL, "Could not create device context", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	int pixelFormat = ChoosePixelFormat(m_hdc, &pfd);
	if (!pixelFormat)
	{
		MessageBox(NULL, "Could not find a suitable pixel format", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	if (!SetPixelFormat(m_hdc, pixelFormat, &pfd))
	{
		MessageBox(NULL, "Could not set the pixel format", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	m_rc = wglCreateContext(m_hdc);
	if (!m_rc)
	{
		MessageBox(NULL, "Could not create a rendering context", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}
	if (!wglMakeCurrent(m_hdc, m_rc))
	{
		MessageBox(NULL, "Could not set rendering context", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return 0;
	}

	ShowWindow(m_wnd, SW_SHOW);
	SetForegroundWindow(m_wnd);
	SetFocus(m_wnd);

	//Initialize OpenGL
	glewExperimental = true;
	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		MessageBox(NULL, "Could not initalize glew", m_title, MB_OK | MB_ICONEXCLAMATION);
		DestroyDisplay();
		return -1;
	}
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return 1;
}
void Application::DestroyDisplay()
{
	if (m_rc)
	{
		if (!wglMakeCurrent(NULL, NULL))
		{
			MessageBox(NULL, "Could not return rendering context", "Game", MB_OK | MB_ICONINFORMATION);
		}
		if (!wglDeleteContext(m_rc))
		{
			MessageBox(NULL, "Could not destroy rendering context", "Game", MB_OK | MB_ICONINFORMATION);
		}
		m_rc = NULL;
	}
	if (m_hdc && !ReleaseDC(m_wnd, m_hdc))
	{
		MessageBox(NULL, "Could not destroy device context", "Game", MB_OK | MB_ICONINFORMATION);
		m_hdc = NULL;
	}
	if (m_wnd && !DestroyWindow(m_wnd))
	{
		MessageBox(NULL, "Could not destroy window", "Game", MB_OK | MB_ICONINFORMATION);
		m_wnd = NULL;
	}
	if (!UnregisterClass("OpenGLWindow", GetModuleHandle(NULL)))
	{
		MessageBox(NULL, "Could not unregister class", "Game", MB_OK | MB_ICONINFORMATION);
	}
}
bool Application::ShouldClose()
{
	bool isRunning = true;
	while (PeekMessage(&m_msg, NULL, 0, 0, PM_REMOVE))
	{
		if (m_msg.message == WM_QUIT)
			isRunning = false;
		else
		{
			TranslateMessage(&m_msg);
			DispatchMessage(&m_msg);
		}
	}
	return isRunning;
}
void Application::SystemInit()
{
	m_systemTime = GetTime();
	float ratio = (float)m_gameWidth / (float)m_gameHeight;
	float windowRatio = (float)m_displayWidth / (float)m_displayHeight;

	if (ratio < windowRatio)
	{
		float newHeight = m_displayWidth / ratio;

		float heightDiff = newHeight - m_displayHeight;
		m_yDisplayExt = heightDiff * 0.5f;
		glViewport(0, -m_yDisplayExt, m_displayWidth, m_displayHeight + heightDiff);

		m_yGameExt = m_yDisplayExt / newHeight * (float)m_gameHeight;
	}
	else
	{
		float newWidth = m_displayHeight * ratio;

		float widthDiff = newWidth - m_displayWidth;
		m_xDisplayExt = widthDiff * 0.5f;
		glViewport(-m_xDisplayExt, 0, m_displayWidth + widthDiff, m_displayHeight);

		m_xGameExt = m_xDisplayExt / newWidth * (float)m_gameWidth;
	}
	Input::GetInstance()->m_app = this;
}
void Application::SystemUpdate()
{
	UpdateTime();
}
void Application::SystemDestroy()
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		delete m_layers[i];
	}
	m_layers.clear();
	Input::DestroyInstance();
	Renderer::DestroyInstance();
	ResourceManager::DestroyInstance();
	CollisionManager::DestroyInstance();
}
void Application::Quit()
{
	PostQuitMessage(0);
}
LRESULT Application::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_SYSCOMMAND:
		{
			switch (wParam)
			{
			case SC_SCREENSAVE:
			case SC_MONITORPOWER:
				return 0;
			}
			break;
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
		case WM_KEYDOWN:
		{
			Input::GetInstance()->SetKey(wParam, true);
			return 0;
		}
		case WM_KEYUP:
		{
			Input::GetInstance()->SetKey(wParam, false);
			return 0;
		}
		case WM_LBUTTONDOWN:
		{
			Input::GetInstance()->SetClicked(true);
			Input::GetInstance()->SetClickPosition(glm::vec2(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
			return 0;
		}
		case WM_MOUSEMOVE:
		{
			if (wParam == MK_LBUTTON)
			{
				Input::GetInstance()->SetClicked(true);
				Input::GetInstance()->SetClickPosition(glm::vec2(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)));
				return 0;
			}
			break;
		}
		case WM_LBUTTONUP:
		{
			Input::GetInstance()->SetClicked(false);
			Input::GetInstance()->SetReleased(true);
			return 0;
		}
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void Application::ChangeScene(Scene * a_scene)
{
	if (m_nextScene)
	{
		delete m_nextScene;
		m_nextScene = nullptr;
	}
	m_nextScene = a_scene;
}
void Application::SceneLoad()
{
	if (m_nextScene)
	{
		if (m_currentScene)
		{
			m_currentScene->UnLoad();
			m_currentScene->ClearScene();
			delete m_currentScene;
			ResourceManager::GetInstance()->Destroy();
			CollisionManager::DestroyInstance();
		}

		m_currentScene = m_nextScene;
		m_currentScene->m_app = this;
		m_currentScene->Load();
		m_currentScene->LoadLayers();
		m_nextScene = nullptr;
	}
}
void Application::SceneUpdate()
{
	if (m_nextScene != nullptr)
		return;

	if (m_currentScene)
	{
		m_currentScene->UpdateLayers(m_deltaTime);
		m_currentScene->Update(m_deltaTime);
		if (m_elapsedTimeStep >= m_timeStep)
		{
			m_currentScene->FixedLayers(m_timeStep);
			m_currentScene->FixedUpdate(m_timeStep);
			CollisionManager::GetInstance()->UpdateCollisions();
			m_elapsedTimeStep -= m_timeStep;
		}
	}
}
void Application::SceneFixedUpdate()
{
	while (m_elapsedTimeStep >= m_timeStep)
	{
		if (m_nextScene == nullptr && m_currentScene)
		{
			m_currentScene->FixedUpdate(m_timeStep);
			m_currentScene->FixedLayers(m_timeStep);
			CollisionManager::GetInstance()->UpdateCollisions();
		}
		FixedUpdate();
		FixedLayers();
		m_elapsedTimeStep -= m_timeStep;
	}
}
void Application::SceneRender()
{
	if (m_nextScene == nullptr && m_currentScene)
	{
		m_currentScene->m_camera.UpdateShader();
		m_currentScene->RenderLayers(m_deltaTime);
		m_currentScene->Render(m_deltaTime);
	}
}
void Application::SceneUnLoad()
{
	if (m_nextScene)
	{
		m_nextScene->UnLoad();
		m_nextScene->ClearScene();
		delete m_nextScene;
		m_nextScene = nullptr;
	}
	if (m_currentScene)
	{
		m_currentScene->UnLoad();
		m_currentScene->ClearScene();
		delete m_currentScene;
		m_currentScene = nullptr;
	}
}

Layer* Application::AddLayer()
{
	m_layers.push_back(new Layer());
	m_layers.back()->SetSceneID(m_layers.size() - 1);
	m_layers.back()->m_app = this;
	return m_layers.back();
}
Layer * Application::GetLayer(int a_index)
{
	if (a_index < m_layers.size())
		return m_layers[a_index];
	return nullptr;
}
void Application::LoadLayers()
{
	m_camera.SetOrtho(0.0f, (float)GetGameWidth(), 0.0f, (float)GetGameHeight());
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Load();
	}
}
void Application::UpdateLayers()
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Update(m_deltaTime);
	}
}
void Application::FixedLayers()
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->FixedUpdate(m_timeStep);
	}
}
void Application::RenderLayers()
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Render(m_camera, m_deltaTime);
	}
}