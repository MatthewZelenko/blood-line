#include "Zengine/windows/Input.h"
#include <memory>
#include "zengine/Application.h"

Input* Input::m_instance = nullptr;

Input::Input() :
	m_clicked(false),
	m_released(false)
{
	memset(m_keyState, 0, sizeof(m_keyState));
	memset(m_oldKeyState, 0, sizeof(m_oldKeyState));
}
Input::~Input()
{
}

Input* Input::GetInstance()
{
	if (!m_instance)
	{
		m_instance = new Input();
	}
	return m_instance;
}

void Input::DestroyInstance()
{
	if (m_instance)
	{
		delete m_instance;
		m_instance = nullptr;
	}
}

void Input::Update()
{
	memcpy(m_oldKeyState, m_keyState, sizeof(m_oldKeyState));
	m_released = false;
}

bool Input::Hide()
{
	return m_keyState[VK_UP] || m_keyState[0x57];
}

bool Input::UnHide()
{
	return m_keyState[VK_DOWN] || m_keyState[0x53];
}

bool Input::ClimbUp()
{
	return m_keyState[VK_UP] || m_keyState[0x57];
}

bool Input::ClimbDown()
{
	return m_keyState[VK_DOWN] || m_keyState[0x53];
}

bool Input::MoveLeft()
{
	return m_keyState[VK_LEFT] || m_keyState[0x41];
}

bool Input::MoveRight()
{
	return m_keyState[VK_RIGHT] || m_keyState[0x44];
}

bool Input::Click(glm::vec2 & a_point)
{
	if (m_clicked)
	{
		a_point = m_clickPosition;
		return true;
	}
	return false;
}

bool Input::Release(glm::vec2 & a_point)
{
	if(m_released)
	{
		a_point = m_clickPosition;
		return true;
	}
	return false;
}

void Input::SetKey(WPARAM a_key, bool a_isDown)
{
	m_keyState[a_key] = a_isDown;
}

void Input::SetClicked(bool a_value)
{
	m_clicked = a_value;
}

void Input::SetClickPosition(const glm::vec2 & a_position)
{
	m_clickPosition = a_position;
	m_clickPosition.y = m_app->GetDisplayHeight() - m_clickPosition.y;

	m_clickPosition = m_clickPosition + glm::vec2(m_app->m_xDisplayExt, m_app->m_yDisplayExt);
	m_clickPosition /= glm::vec2(m_app->GetDisplayWidth(), m_app->GetDisplayHeight()) + glm::vec2(m_app->m_xDisplayExt * 2.0f, m_app->m_yDisplayExt * 2.0f);
	m_clickPosition *= glm::vec2(m_app->GetGameWidth(), m_app->GetGameHeight());
}

void Input::SetReleased(bool a_value)
{
	m_released = a_value;
}
