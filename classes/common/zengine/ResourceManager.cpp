#include "ResourceManager.h"

ResourceManager* ResourceManager::m_instance = nullptr;

ResourceManager::ResourceManager()
{
}
ResourceManager::~ResourceManager()
{
}

ResourceManager* ResourceManager::GetInstance()
{
	if (!m_instance)
		m_instance = new ResourceManager();
	return m_instance;
}
void ResourceManager::DestroyInstance()
{
	if (m_instance)
	{
		m_instance->Destroy(true);
		delete m_instance;
	}
	m_instance = nullptr;
}
void ResourceManager::Destroy(bool a_all)
{
	std::map<std::string, TextureStruct>::iterator tIter = m_textures.begin();
	while (tIter != m_textures.end())
	{	
		if (a_all || !tIter->second.m_keep)
		{
			tIter->second.m_texture.Destroy();
			tIter = m_textures.erase(tIter);
		}
		else
		{
			++tIter;
		}
	}
	std::map<std::string, ShaderStruct>::iterator sIter = m_shaders.begin();
	while (sIter != m_shaders.end())
	{
		if (a_all || !sIter->second.m_keep)
		{
			sIter->second.m_shader.Destroy();
			sIter = m_shaders.erase(sIter);
		}
		else
		{
			++sIter;
		}
	}
	std::map<std::string, AnimationStruct>::iterator aIter = m_animations.begin();
	while (aIter != m_animations.end())
	{
		if (a_all || !aIter->second.m_keep)
		{
			aIter = m_animations.erase(aIter);
		}
		else
		{
			++aIter;
		}
	}
}

Texture* ResourceManager::CreateTexture(std::string a_textureName, bool a_keep)
{
	m_textures[a_textureName].m_keep = a_keep;
	return &m_textures[a_textureName].m_texture;
}
Shader* ResourceManager::CreateShader(std::string a_shaderName, bool a_keep)
{
	m_shaders[a_shaderName].m_keep = a_keep;
	return &m_shaders[a_shaderName].m_shader;
}
Animation* ResourceManager::CreateAnimation(std::string a_animationName, bool a_keep)
{
	m_animations[a_animationName].m_keep = a_keep;
	return &m_animations[a_animationName].m_animation;
}

Texture* ResourceManager::GetTexture(std::string a_textureName)
{
	if (m_textures.find(a_textureName) == m_textures.end())
		return nullptr;
	return &m_textures[a_textureName].m_texture;
}
Shader* ResourceManager::GetShader(std::string a_shaderName)
{
	if (m_shaders.find(a_shaderName) == m_shaders.end())
		return nullptr;
	return &m_shaders[a_shaderName].m_shader;
}
Animation* ResourceManager::GetAnimation(std::string a_animationName)
{
	if (m_animations.find(a_animationName) == m_animations.end())
		return nullptr;
	return &m_animations[a_animationName].m_animation;
}