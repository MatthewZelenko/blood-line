#ifndef GAME_LOG_H
#define GAME_LOG_H
#include <string>

class Log
{
public:
    static void Info(const char* a_message, ...);
    static void Error(const char* a_message, ...);
    static void Warning(const char* a_message, ...);
    static void Debug(const char* a_message, ...);
    static std::string IntToString(int a_value);
};
#endif //GAME_LOG_H