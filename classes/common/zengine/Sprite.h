#pragma once
#include <GLM/glm.hpp>
#include "Renderer.h"
#include "Shader.h"
#include "Texture.h"
#include "Component.h"
#include "Animation.h"

class Texture;
class Sprite : public Component
{
public:
	Sprite();
	~Sprite();

	void Animate(float a_deltaTime);
	void AddAnimation(Animation* a_animation);
	void ChangeAnimation(int a_index, bool a_reset = false);

	void FramesChangeSize(bool a_value) { m_framesChangeSize = a_value; }
	void FramesChangeColliderSize(bool a_value) { m_framesChangeColliderSize = a_value; }
	void SetShader(Shader* a_shader) { m_shader = a_shader; }
	void SetTexture(Texture* a_texture) { m_texture = a_texture; }
	void SetSource(const glm::vec4& a_source);
	void SetFixedSource(const glm::vec4& a_source);
	void SetColour(const glm::vec4& a_colour) { m_colour = a_colour; }
	void SetFlippedX(bool a_value);
	void SetAnimationOffset(const glm::vec2& a_offset);
	void SetOrigin(const glm::vec2& a_offset);

	Shader GetShader() { return *m_shader; }
	Texture GetTexture() { return *m_texture; }
	glm::vec4 GetSource() { return m_source; }
	glm::vec4 GetColour() { return m_colour; }
	glm::ivec2 GetFlipped() { return m_flipped; }
	glm::vec2 GetOrigin(){ return m_origin; }
	glm::vec2 GetAnimationOffset() { return m_animationOffset; }
	glm::mat4 GetTextureMat() { return m_textureMat; }
	int GetCurrentAnimation() { return m_currentAnimation; }
	int FinishedAnimation();
	void CalculateMat();

protected:
	Shader* m_shader;
	Texture* m_texture;
	glm::mat4 m_textureMat;
	glm::vec4 m_source;
	glm::vec4 m_colour;
	glm::ivec2 m_flipped;
	glm::vec2 m_animationOffset;
	glm::vec2 m_origin;

private:
	float m_animationElapsed;
	int m_currentAnimation;
	std::vector<Animation*> m_animations;
	int m_animationSourceIndex;
	int m_loop;
	bool m_framesChangeSize;
	bool m_framesChangeColliderSize;
};