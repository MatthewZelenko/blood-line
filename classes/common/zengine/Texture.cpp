#define STB_IMAGE_IMPLEMENTATION
#include "Zengine/Texture.h"
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#include <android_native_app_glue.h>
#include "Application.h"
#else
#include <GLEW/glew.h>
#endif
#include <STB/stb_image.h>
#include <Zengine/Log.h>

Texture::Texture():
	m_id(0),
	m_width(0),
	m_height(0),
	m_comp(STBI_rgb),
	m_inverseWidth(0.0f),
	m_inverseHeight(0.0f)
{

}
Texture::~Texture()
{

}
#ifdef __ANDROID__
void Texture::Load(const char* a_path)
{
	Destroy();
	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);

	int comp = STBI_rgb;

	AAsset* img = AAssetManager_open(Application::m_application->activity->assetManager, a_path, AASSET_MODE_UNKNOWN);
	if(!img)
	{
		Destroy();
		Log::Error("Could Not Load Image: %s", a_path);
		return;
	}
	size_t size = AAsset_getLength(img);

	unsigned char* data = stbi_load_from_memory((unsigned char*)AAsset_getBuffer(img), size, &m_width, &m_height, &comp, 0);

	if (data)
	{
		if (comp == STBI_rgb)
			comp = GL_RGB;
		else
			comp = GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, comp, m_width, m_height, 0, comp, GL_UNSIGNED_BYTE, data);
		m_inverseWidth = 1.0f / m_width;
		m_inverseHeight = 1.0f / m_height;
	}
	else
	{
		Destroy();
		Log::Error("Could not load image: %\n", a_path);
	}
	stbi_image_free(data);
}
#else
void Texture::Load(const char* a_path)
{
	Destroy();
	glGenTextures(1, &m_id);
	glBindTexture(GL_TEXTURE_2D, m_id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);

	int comp = STBI_rgb;

	unsigned char* data = stbi_load(a_path, &m_width, &m_height, &comp, 0);

	if (data)
	{
		if (comp == STBI_rgb)
			comp = GL_RGB;
		else
			comp = GL_RGBA;
		glTexImage2D(GL_TEXTURE_2D, 0, comp, m_width, m_height, 0, (GLenum)comp, GL_UNSIGNED_BYTE, data);
		m_inverseWidth = 1.0f / m_width;
		m_inverseHeight = 1.0f / m_height;
	}
	else
	{
		Destroy();
		Log::Error("Could not load image: %s\n", a_path);
	}
	stbi_image_free(data);
}
#endif
void Texture::Bind(int a_location)
{
	if (!m_id)
		return;
	glActiveTexture((GLenum)(GL_TEXTURE0 + a_location));
	glBindTexture(GL_TEXTURE_2D, m_id);
}

void Texture::Destroy()
{
	m_width = 0;
	m_height = 0;
	m_comp = STBI_rgb;
	if (m_id)
	{
		glDeleteTextures(1, &m_id);
		m_id = 0;
	}
}