#include "zengine/Scene.h"
#include "zengine/ResourceManager.h"
#include "zengine/CollisionManager.h"

Scene::Scene()
{
}
Scene::~Scene()
{
}
void Scene::ClearScene()
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		delete m_layers[i];
	}
	m_layers.clear();
}

void Scene::Load()
{
}
void Scene::Update(float a_deltaTime)
{
}
void Scene::FixedUpdate(float a_fixedTime)
{
}
void Scene::Render(float a_deltaTime)
{
}
void Scene::UnLoad()
{
}

void Scene::SetName(std::string a_name)
{
	m_name = a_name;
}

Layer* Scene::AddLayer()
{
	m_layers.push_back(new Layer());
	m_layers.back()->SetSceneID(m_layers.size() - 1);
	m_layers.back()->m_app = m_app;
	return m_layers.back();
}

Layer * Scene::GetLayer(int a_index)
{
	if (a_index < m_layers.size())
		return m_layers[a_index];
	return nullptr;
}

void Scene::LoadLayers()
{
	m_camera.SetOrtho(0.0f, (float)m_app->GetGameWidth(), 0.0f, (float)m_app->GetGameHeight());
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Load();
	}
}

void Scene::UpdateLayers(float a_deltaTime)
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Update(a_deltaTime);
	}
}

void Scene::FixedLayers(float a_fixedTime)
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->FixedUpdate(a_fixedTime);
	}
}

void Scene::RenderLayers(float a_deltaTime)
{
	for (int i = 0; i < m_layers.size(); i++)
	{
		m_layers[i]->Render(m_camera, a_deltaTime);
	}
}
