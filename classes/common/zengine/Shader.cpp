#include "Zengine/Shader.h"

#include "Zengine/Log.h"
#include <GLM/gtc/type_ptr.hpp>
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#include <android_native_app_glue.h>
#include "Application.h"
#else
#include <sstream>
#include <fstream>
#include <GLEW/glew.h>
#endif

Shader::Shader():
m_id(0)
{
}
Shader::~Shader()
{
}
void Shader::Create()
{
    m_id = glCreateProgram();
}
void Shader::Destroy()
{
	if (m_id)
	{
		glDeleteProgram(m_id);
		m_id = 0;
	}
}
void Shader::AttachShaderFromString(std::string a_shader, unsigned int a_shaderType)
{
    if (a_shader.empty())
        return;

#ifdef __ANDROID__
    a_shader.insert(0, "#version 300 es\r\n");
#else
    a_shader.insert(0, "#version 330\r\n");
#endif
    const char* s = a_shader.c_str();

    int shader = glCreateShader(a_shaderType);
    glShaderSource(shader, 1, &s, 0);
    glCompileShader(shader);
    int success = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (success != GL_TRUE)
    {
        char infoLog[512];
        glGetShaderInfoLog(shader, 512, 0, infoLog);
		infoLog[511] = '\0';
        Log::Error("Shader(Type=%i): %s", a_shaderType, infoLog);
        glDeleteShader(shader);
        return;
    }
    glAttachShader(m_id, shader);
    m_shaders.push_back(shader);
}
void Shader::AttachShaderFromFile(std::string a_shader, unsigned int a_shaderType)
{
	bool success = true;
#ifdef __ANDROID__
	AAsset* file = AAssetManager_open(Application::m_application->activity->assetManager, a_shader.c_str(), AASSET_MODE_UNKNOWN);
	size_t size = AAsset_getLength(file);
	char* content = new char[size + 1];
	AAsset_read(file, content, size);
    content[size] = '\0';
	AttachShaderFromString(content, a_shaderType);
	delete[] content;
#else
	std::ifstream file;
	std::stringstream stream;

	file.open(a_shader);
	if (!file.good())
	{
		Log::Error("Shader::Vertex: Could not read file: %s", a_shader);
		success = false;
	}

	stream << file.rdbuf();
	std::string content = stream.str().c_str();
	file.close();
	AttachShaderFromString(content.c_str(), a_shaderType);
#endif
}
void Shader::LinkProgram()
{
    glLinkProgram(m_id);

    int success = 0;
    glGetProgramiv(m_id, GL_LINK_STATUS, &success);
    if (success != GL_TRUE)
    { 
        char infoLog[512];
        glGetProgramInfoLog(m_id, 512, NULL, infoLog);
		infoLog[511] = '/0';
        Log::Error("Program: %s", infoLog);
		//std::printf("Shader: %s", infoLog);
    }

    for (int i = 0; i < m_shaders.size(); i++)
    {
        glDeleteShader(m_shaders[i]);
    }
    m_shaders.clear();
    glUseProgram(0);
}

void Shader::Use()
{
    glUseProgram(m_id);
}

void Shader::UnUse()
{
    glUseProgram(0);
}

void Shader::BindLocation(char* a_name, int a_location)
{
	m_locations[a_name] = a_location;
    glBindAttribLocation(m_id, a_location, a_name);
}
void Shader::SetMatrix4x4(char* a_location, const glm::mat4& a_mat)
{
    if(m_locations.find(a_location) == m_locations.end())
    {
        m_locations[a_location] = glGetUniformLocation(m_id, a_location);
    }
    glUniformMatrix4fv(m_locations[a_location], 1, GL_FALSE, glm::value_ptr(a_mat));
}

void Shader::SetVec4(char * a_location, const glm::vec4& a_vec)
{
	if (m_locations.find(a_location) == m_locations.end())
	{
		m_locations[a_location] = glGetUniformLocation(m_id, a_location);
	}
	glUniform4fv(m_locations[a_location], 1, glm::value_ptr(a_vec));
}

void Shader::SetIVec2(char * a_location, const glm::ivec2& a_vec)
{
	if (m_locations.find(a_location) == m_locations.end())
	{
		m_locations[a_location] = glGetUniformLocation(m_id, a_location);
	}
	glUniform2iv(m_locations[a_location], 1, glm::value_ptr(a_vec));
}
