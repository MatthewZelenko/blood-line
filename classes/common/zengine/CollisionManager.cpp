#include "CollisionManager.h"
#include "ResourceManager.h"
#include <GLM/gtc/matrix_transform.hpp>

CollisionManager* CollisionManager::m_instance = nullptr;

CollisionManager::CollisionManager()
{
}
CollisionManager::~CollisionManager()
{
}

CollisionManager * CollisionManager::GetInstance()
{
	if (!m_instance)
	{
		m_instance = new CollisionManager();
	}
	return m_instance;
}

void CollisionManager::DestroyInstance()
{
	if (m_instance)
	{
		delete m_instance;
		m_instance = nullptr;
	}
}

void CollisionManager::Register(Pawn * a_pawn)
{
	int id = 0;
	if (m_freeIndices.empty())
	{
		id = m_pawns.size();
		m_pawns.push_back(a_pawn);
	}
	else
	{
		id = m_freeIndices.back();
		m_freeIndices.pop_back();
		m_pawns[id] = a_pawn;
	}
	a_pawn->m_collider.SetColliderId(id);
}
void CollisionManager::UnRegister(int a_index)
{
	m_pawns[a_index]->m_collider.SetColliderId(-1);
	m_pawns[a_index] = nullptr;
	m_freeIndices.push_back(a_index);
}
void CollisionManager::UpdateCollisions()
{
	for (int i = 0; i < m_pawns.size(); ++i)
	{
		if (m_pawns[i] == nullptr || !m_pawns[i]->m_collider.m_isActive)
			continue;
		for (int j = i + 1; j < m_pawns.size(); ++j)
		{
			if (m_pawns[j] == nullptr || !m_pawns[j]->m_collider.m_isActive)
				continue;

			CheckCollision(i, m_pawns[i], j, m_pawns[j]);
		}
	}
}
void CollisionManager::CheckCollision(int a_i, Pawn * a_pawn1, int a_j, Pawn * a_pawn2)
{
	glm::vec2 min1 = (glm::vec2(a_pawn1->m_sprite.GetFlipped().x ? -1 : 1, a_pawn1->m_sprite.GetFlipped().y ? -1 : 1) * a_pawn1->m_collider.GetPosition()) + (a_pawn1->m_transform.GetPosition() - glm::vec2(a_pawn1->m_sprite.GetFlipped().x ? 1 : 0, a_pawn1->m_sprite.GetFlipped().y ? 1 : 0) * a_pawn1->m_collider.GetSize());
	glm::vec2 max1 = min1 + a_pawn1->m_collider.GetSize();

	glm::vec2 min2 = (glm::vec2(a_pawn2->m_sprite.GetFlipped().x ? -1 : 1, a_pawn2->m_sprite.GetFlipped().y ? -1 : 1) * a_pawn2->m_collider.GetPosition()) + (a_pawn2->m_transform.GetPosition() - glm::vec2(a_pawn2->m_sprite.GetFlipped().x ? 1 : 0, a_pawn2->m_sprite.GetFlipped().y ? 1 : 0) * a_pawn2->m_collider.GetSize());
	glm::vec2 max2 = min2 + a_pawn2->m_collider.GetSize();

	if (a_pawn1->m_collider.m_colliding.size() < a_j + 1)
	{
		a_pawn1->m_collider.m_colliding.resize(a_j + 1);
		a_pawn1->m_collider.m_colliding[a_j] = false;
	}
	if (a_pawn2->m_collider.m_colliding.size() < a_i + 1)
	{
		a_pawn2->m_collider.m_colliding.resize(a_i + 1);
		a_pawn2->m_collider.m_colliding[a_i] = false;
	}

	bool trigger = a_pawn1->m_collider.m_isTrigger || a_pawn2->m_collider.m_isTrigger;
	if (min1.x < max2.x &&
		max1.x > min2.x &&
		min1.y < max2.y &&
		max1.y > min2.y)
	{
		if (!trigger)
		{
			ResolveCollision(a_pawn1, min1, max1, a_pawn2, min2, max2);
		}
		if (a_pawn1->m_collider.m_colliding[a_j])
		{
			if (trigger)
			{
				a_pawn1->OnTriggerStay(a_pawn2);
				a_pawn2->OnTriggerStay(a_pawn1);
			}
			else
			{
				a_pawn1->OnCollisionStay(a_pawn2);
				a_pawn2->OnCollisionStay(a_pawn1);
			}
		}
		else
		{
			if (trigger)
			{
				a_pawn1->OnTriggerEnter(a_pawn2);
				a_pawn2->OnTriggerEnter(a_pawn1);
			}
			else
			{
				a_pawn1->OnCollisionEnter(a_pawn2);
				a_pawn2->OnCollisionEnter(a_pawn1);
			}
			a_pawn1->m_collider.m_colliding[a_j] = true;
			a_pawn2->m_collider.m_colliding[a_i] = true;
		}
	}
	else
	{
		if (a_pawn1->m_collider.m_colliding[a_j])
		{
			if (trigger)
			{
				a_pawn1->OnTriggerExit(a_pawn2);
				a_pawn2->OnTriggerExit(a_pawn1);
			}
			else
			{
				a_pawn1->OnCollisionExit(a_pawn2);
				a_pawn2->OnCollisionExit(a_pawn1);
			}
			a_pawn1->m_collider.m_colliding[a_j] = false;
			a_pawn2->m_collider.m_colliding[a_i] = false;
		}
	}
}

void CollisionManager::ResolveCollision(Pawn * a_pawn1, const glm::vec2 & a_min1, const glm::vec2 & a_max1, Pawn * a_pawn2, const glm::vec2 & a_min2, const glm::vec2 & a_max2)
{
	float min;
	glm::vec2 axis;

	if (a_max2.x > a_min1.x)
	{
		float x = std::abs(a_max2.x - a_min1.x);
		min = x;
		axis = glm::vec2(1, 0);
	}
	if (a_max1.x > a_min2.x)
	{
		float x = std::abs(a_max1.x - a_min2.x);
		if (x < min)
		{
			min = x;
			axis = glm::vec2(-1, 0);
		}
	}
	if (a_max2.y > a_min1.y)
	{
		float y = std::abs(a_max2.y - a_min1.y);
		if (y < min)
		{
			min = y;
			axis = glm::vec2(0, 1);
		}
	}
	if (a_max1.y > a_min2.y)
	{
		float y = std::abs(a_max1.y - a_min2.y);
		if (y < min)
		{
			min = y;
			axis = glm::vec2(0, -1);
		}
	}
	int isStatic = (a_pawn1->IsStatic() ? 1 : 0) + (a_pawn2->IsStatic() ? 2 : 0);
	if (isStatic != 0)
	{
		if(isStatic == 1)
			a_pawn2->m_transform.Translate(-min * axis);
		else if(isStatic == 2)
			a_pawn1->m_transform.Translate(min * axis);
	}
	else
	{
		a_pawn1->m_transform.Translate(min * axis * 0.5f);
		a_pawn2->m_transform.Translate(-min * axis * 0.5f);

	}
}

void CollisionManager::Render()
{
	Texture* texture = ResourceManager::GetInstance()->GetTexture("Square");
	Shader* shader = ResourceManager::GetInstance()->GetShader("normal");
	for (int i = 0; i < m_pawns.size(); ++i)
	{
		if (m_pawns[i] == nullptr)
			continue;
		
		glm::vec2 pos = glm::vec2(m_pawns[i]->m_sprite.GetFlipped().x ? -1 : 1, m_pawns[i]->m_sprite.GetFlipped().y ? -1 : 1) * m_pawns[i]->m_collider.GetPosition() + m_pawns[i]->m_transform.GetPosition();
		glm::vec2 posSize = glm::vec2(m_pawns[i]->m_sprite.GetFlipped().x ? 1 : 0, m_pawns[i]->m_sprite.GetFlipped().y ? 1 : 0) * m_pawns[i]->m_collider.GetSize();

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(pos - posSize, 0.0f));
		model = glm::scale(model, glm::vec3(m_pawns[i]->m_collider.GetSize(), 1.0f));

		Renderer::GetInstance()->Render(*texture, *shader, model, glm::mat4(), glm::vec4(1.0f));
	}
}