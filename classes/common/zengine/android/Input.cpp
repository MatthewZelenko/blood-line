#include "Input.h"
#include "zengine/Application.h"

Input *Input::m_instance = nullptr;

Input::Input() :
        m_deadZone(glm::vec2(16.0f, 8.0f)),
        m_released(false)
{

}

Input::~Input()
{
}

Input *Input::GetInstance()
{
    if (!m_instance)
    {
        m_instance = new Input();
    }
    return m_instance;
}

void Input::DestroyInstance()
{
    if (m_instance)
    {
        delete m_instance;
        m_instance = nullptr;
    }
}

void Input::Update()
{
    m_released = false;
}

bool Input::Hide()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_lastPosition;
        if (direction.y > m_deadZone.y)
            return true;
    }
    return false;
}

bool Input::UnHide()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_lastPosition;
        if (direction.y < -m_deadZone.y)
            return true;
    }
    return false;
}

bool Input::ClimbUp()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_startPosition;
        if (direction.y > m_deadZone.y)
            return true;
    }
    return false;
}

bool Input::ClimbDown()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_startPosition;
        if (direction.y < -m_deadZone.y)
            return true;
    }
    return false;
}

bool Input::MoveLeft()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_startPosition;
            if (direction.x < -m_deadZone.x)
                return true;
    }
    return false;
}

bool Input::MoveRight()
{
    if (m_touch)
    {
        glm::vec2 direction = m_currentPosition - m_startPosition;
            if (direction.x > m_deadZone.x)
                return true;
    }
    return false;
}
bool Input::Click(glm::vec2& a_point)
{
    if(m_touch)
    {
        a_point = m_currentPosition;
        return true;
    }
    return false;
}
bool Input::Release(glm::vec2 & a_point)
{
    if (m_released)
    {
        a_point = m_currentPosition;
        return true;
    }
    return false;
}

void Input::SetStartPosition(const glm::vec2 &a_pos)
{
    m_startPosition = a_pos;
    m_startPosition.y = m_app->GetDisplayHeight() - m_startPosition.y;

    m_startPosition /= glm::vec2(m_app->GetDisplayWidth(), m_app->GetDisplayHeight());
    m_startPosition *= glm::vec2(m_app->GetGameWidth(), m_app->GetGameHeight());
}

void Input::SetCurrentPosition(const glm::vec2 &a_pos)
{
    m_currentPosition = a_pos;
    m_currentPosition.y = m_app->GetDisplayHeight() - m_currentPosition.y;

    m_currentPosition /= glm::vec2(m_app->GetDisplayWidth(), m_app->GetDisplayHeight());
    m_currentPosition *= glm::vec2(m_app->GetGameWidth(), m_app->GetGameHeight());
}

void Input::SetLastPosition(const glm::vec2& a_pos)
{
    m_lastPosition = a_pos;
}
void Input::IsTouching(bool a_value)
{
    m_touch = a_value;
}
void Input::SetReleased(bool a_value)
{
    m_released = a_value;
}