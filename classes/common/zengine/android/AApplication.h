#pragma once
#include "android_native_app_glue.h"
#include <EGL/egl.h>
#include <sys/time.h>
#include <zengine/Layer.h>

class Scene;
class Application
{
public:
	Application();
	~Application();

	void Init(android_app* a_app, int a_gameWidth, int a_gameHeight);

	void Run();

	virtual void Load();
	virtual void Update();
	virtual void FixedUpdate();
	virtual void Render();
	virtual void UnLoad();

	void ChangeScene(Scene* a_scene);

	void Quit();

	int GetDisplayWidth(){return m_displayWidth;}
	int GetDisplayHeight(){return m_displayHeight;}
	int GetGameWidth(){return m_gameWidth;}
	int GetGameHeight(){return m_gameHeight;}
	float GetScreenLeft() { return m_xGameExt; }
	float GetScreenRight() { return m_gameWidth - m_xGameExt; }
	float GetScreenBottom() { return m_yGameExt; }
	float GetScreenTop() { return (m_gameHeight - m_yGameExt); }
	float GetDeltaTime(){return m_deltaTime;}
	float GetElapsedTime(){ return  m_elapsedTime;}

	float m_xDisplayExt, m_yDisplayExt;
	float m_xGameExt, m_yGameExt;
	static android_app* m_application;

	Layer* AddLayer();
	Layer* GetLayer(int a_index);
private:
	void SystemInit();
	void SystemUpdate();
	void SystemDestroy();
	int CreateDisplay();
	void DestroyDisplay();
	timeval GetTime();
	void UpdateTime();

	static void OnAppCmd(android_app* a_app, int32_t a_cmd);
	static int32_t OnInputEvent(android_app* a_app, AInputEvent* a_event);

    timeval m_systemTime;

	EGLDisplay m_display;
	EGLSurface m_surface;
	EGLContext m_context;

	int m_displayWidth, m_displayHeight;
	int m_gameWidth, m_gameHeight;
	float m_deltaTime, m_elapsedTime;
	float m_elapsedTimeStep, m_timeStep;

	//SCENE
	void SceneLoad();
	void SceneUpdate();
	void SceneFixedUpdate();
	void SceneRender();
	void SceneUnLoad();

	Scene* m_currentScene;
	Scene* m_nextScene;

	void LoadLayers();
	void UpdateLayers();
	void FixedLayers();
	void RenderLayers();
	std::vector<Layer*> m_layers;
	Camera m_camera;
};