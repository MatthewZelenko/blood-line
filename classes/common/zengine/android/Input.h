#pragma once
#include <GLM/glm.hpp>

class Application;
class Input
{
public:
	Input();
	~Input();

	static Input* GetInstance();
	static void DestroyInstance();

	void Update();
	
	bool Hide();
	bool UnHide();
	bool ClimbUp();
	bool ClimbDown();
	bool MoveLeft();
	bool MoveRight();
	bool Click(glm::vec2& a_point);
	bool Release(glm::vec2& a_point);

	glm::vec2 GetCurrentPosition(){return m_currentPosition;}

	void SetStartPosition(const glm::vec2& a_pos);
	void SetCurrentPosition(const glm::vec2& a_pos);
	void SetLastPosition(const glm::vec2& a_pos);
	void IsTouching(bool a_value);
	void SetReleased(bool a_value);

private:
	friend class Application;
	static Input* m_instance;

	Application* m_app;
	glm::vec2 m_deadZone;
	bool m_touch;
	bool m_released;
	glm::vec2 m_startPosition, m_currentPosition, m_lastPosition;
};