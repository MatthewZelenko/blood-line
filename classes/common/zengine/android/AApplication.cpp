#include "AApplication.h"
#include <GLES3/gl3.h>
#include "zengine/Log.h"
#include "Input.h"
#include "zengine/Renderer.h"
#include "zengine/ResourceManager.h"
#include "zengine/Scene.h"
#include "zengine/CollisionManager.h"

android_app *Application::m_application = nullptr;

Application::Application() :
m_display(EGL_NO_DISPLAY), m_context(EGL_NO_CONTEXT), m_surface(EGL_NO_SURFACE),
m_gameWidth(0), m_gameHeight(0),
m_displayWidth(0), m_displayHeight(0),
m_elapsedTimeStep(0.0f),
m_timeStep(0.01f),
m_deltaTime(0.0f),
m_currentScene(nullptr),
m_nextScene(nullptr),
m_xDisplayExt(0.0f),
m_yDisplayExt(0.0f),
m_xGameExt(0.0f),
m_yGameExt(0.0f)
{
    m_application = nullptr;
    m_systemTime = GetTime();
}

Application::~Application()
{
}

void Application::Run()
{
    app_dummy();
    int32_t events;
    android_poll_source *source;
    Log::Info("Main Loop Started");
    m_application->onAppCmd = OnAppCmd;
    m_application->onInputEvent = OnInputEvent;
    while (true)
    {
        SystemUpdate();
        while (ALooper_pollAll(0, NULL, &events, (void **) &source) >= 0)
        {
            if (source != NULL)
            {
                source->process(m_application, source);
            }
            if (m_application->destroyRequested)
            {
                return;
            }
        }
        SceneLoad();
        SceneUpdate();
        Update();
        UpdateLayers();
        SceneFixedUpdate();
        if (m_display != NULL && !m_nextScene)
        {
            glClear(GL_COLOR_BUFFER_BIT);
            SceneRender();
            Render();
            RenderLayers();
            if (eglSwapBuffers(m_display, m_surface) != GL_TRUE)
                Log::Error("Can not swap Buffers");
        }
        Input::GetInstance()->Update();
    }
}

void Application::Init(android_app* a_app, int a_gameWidth, int a_gameHeight)
{
    m_application = a_app;
    m_gameWidth = a_gameWidth;
    m_gameHeight = a_gameHeight;
    m_application->userData = this;
}

void Application::Load()
{
}
void Application::Update()
{
}
void Application::FixedUpdate()
{
}
void Application::Render()
{
}
void Application::UnLoad()
{
}

timeval Application::GetTime()
{
    timeval time;
    gettimeofday(&time, NULL);
    return time;
}
void Application::UpdateTime()
{
    timeval newTime;
    gettimeofday(&newTime, NULL);
    m_deltaTime = (newTime.tv_sec - m_systemTime.tv_sec) * 1000.0;
    m_deltaTime += (newTime.tv_usec - m_systemTime.tv_usec) * 0.001;
    m_deltaTime *= 0.001f;
    m_systemTime = newTime;
    m_elapsedTime += m_deltaTime;
    m_elapsedTimeStep += m_deltaTime;
}
int Application::CreateDisplay()
{
    const EGLint attribs[] = {
    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    EGL_BLUE_SIZE, 8,
    EGL_GREEN_SIZE, 8,
    EGL_RED_SIZE, 8,
    EGL_NONE
    };
    const EGLint contextAttribs[] = {
    EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE
    };
    EGLint format;
    EGLint numConfigs;
    EGLConfig config;

    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (m_display == EGL_NO_DISPLAY)
        Log::Error("NO Display");
    eglInitialize(m_display, NULL, NULL);
    if (!eglChooseConfig(m_display, attribs, &config, 1, &numConfigs) || numConfigs <= 0)
        Log::Error("NO Configs");
    if (!eglGetConfigAttrib(m_display, config, EGL_NATIVE_VISUAL_ID, &format))
        Log::Error("NO Get Attribs");
    ANativeWindow_setBuffersGeometry(m_application->window, 0, 0, format);

    m_surface = eglCreateWindowSurface(m_display, config, m_application->window, NULL);
    if (m_surface == EGL_NO_SURFACE)
        Log::Error("NO Surface");
    m_context = eglCreateContext(m_display, config, NULL, contextAttribs);
    if (m_context == EGL_NO_CONTEXT)
        Log::Error("NO Context");

    if (!eglMakeCurrent(m_display, m_surface, m_surface, m_context) ||
        !eglQuerySurface(m_display, m_surface, EGL_WIDTH, &m_displayWidth) ||
        !eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &m_displayHeight) ||
        m_displayWidth <= 0 || m_displayHeight <= 0)
    {
        Log::Error("EGL NO MAKE");
    }

    //GL
    glDisable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    return 0;
}
void Application::DestroyDisplay()
{
    if (m_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (m_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(m_display, m_context);
            m_context = EGL_NO_CONTEXT;
        }
        if (m_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(m_display, m_surface);
            m_surface = EGL_NO_SURFACE;
        }
        eglTerminate(m_display);
        m_display = EGL_NO_DISPLAY;
    }
    SystemDestroy();
}
void Application::SystemInit()
{

    m_systemTime = GetTime();
    float ratio = (float)m_gameWidth / (float)m_gameHeight;
    float windowRatio = (float)m_displayWidth / (float)m_displayHeight;

    if (ratio < windowRatio)
    {
        float newHeight = m_displayWidth / ratio;

        float heightDiff = newHeight - m_displayHeight;
        m_yDisplayExt = heightDiff * 0.5f;
        glViewport(0, -m_yDisplayExt, m_displayWidth, m_displayHeight + heightDiff);

        m_yGameExt = m_yDisplayExt / newHeight * (float)m_gameHeight;
    }
    else
    {
        float newWidth = m_displayHeight * ratio;

        float widthDiff = newWidth - m_displayWidth;
        m_xDisplayExt = widthDiff * 0.5f;
        glViewport(-m_xDisplayExt, 0, m_displayWidth + widthDiff, m_displayHeight);

        m_xGameExt = m_xDisplayExt / newWidth * (float)m_gameWidth;
    }
    Input::GetInstance()->m_app = this;
    Load();
}
void Application::SystemUpdate()
{
    UpdateTime();
}
void Application::SystemDestroy()
{
    for (int i = 0; i < m_layers.size(); i++)
    {
        delete m_layers[i];
    }
    m_layers.clear();
    ResourceManager::DestroyInstance();
    Renderer::DestroyInstance();
    Input::DestroyInstance();
    CollisionManager::DestroyInstance();
}
void Application::OnAppCmd(android_app *a_app, int32_t a_cmd)
{
    Application* app = (Application *) a_app->userData;
    switch (a_cmd)
    {
        case APP_CMD_INIT_WINDOW:
        {
            app->CreateDisplay();
            app->SystemInit();
            app->Load();
            app->LoadLayers();
            AConfiguration_setOrientation(a_app->config, ACONFIGURATION_ORIENTATION_LAND);
            break;
        }
        case APP_CMD_TERM_WINDOW:
        {
            app->SceneUnLoad();
            app->UnLoad();
            app->SystemDestroy();
            app->DestroyDisplay();
        }
            break;
        case APP_CMD_START:
            break;
        case APP_CMD_RESUME:
            break;
        case APP_CMD_PAUSE:
            break;
        case APP_CMD_STOP:
            break;
        default:
            break;
    }
}
int32_t Application::OnInputEvent(android_app *a_app, AInputEvent *a_event)
{
    int32_t eventType = AInputEvent_getType(a_event);
    switch (eventType)
    {
        case AINPUT_EVENT_TYPE_MOTION:
        {
            switch (AInputEvent_getSource(a_event))
            {
                case AINPUT_SOURCE_TOUCHSCREEN:
                {
                    int action = AKeyEvent_getAction(a_event) & AMOTION_EVENT_ACTION_MASK;
                    switch (action)
                    {
                        case AMOTION_EVENT_ACTION_DOWN:
                        {
                            Input::GetInstance()->IsTouching(true);
                            glm::vec2 pos = glm::vec2(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0));
                            Input::GetInstance()->SetStartPosition(pos);
                            Input::GetInstance()->SetCurrentPosition(pos);
                            Input::GetInstance()->SetLastPosition(Input::GetInstance()->GetCurrentPosition());
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_UP:
                        {
                            Input::GetInstance()->IsTouching(false);
                            Input::GetInstance()->SetReleased(true);
                            return 1;
                        }
                        case AMOTION_EVENT_ACTION_MOVE:
                        {
                            Input::GetInstance()->SetLastPosition(Input::GetInstance()->GetCurrentPosition());
                            Input::GetInstance()->SetCurrentPosition(glm::vec2(AMotionEvent_getX(a_event, 0), AMotionEvent_getY(a_event, 0)));
                            return 1;
                        }

                    }
                    break;
                }
            }
            break;
        }
    }
    return 0;
}
void Application::Quit()
{
    ANativeActivity_finish(m_application->activity);
}

//SCENE
void Application::ChangeScene(Scene * a_scene)
{
    if (m_nextScene)
    {
        delete m_nextScene;
        m_nextScene = nullptr;
    }
    m_nextScene = a_scene;
}
void Application::SceneLoad()
{
    if (m_nextScene)
    {
        if (m_currentScene)
        {
            m_currentScene->UnLoad();
            m_currentScene->ClearScene();
            delete m_currentScene;
            ResourceManager::GetInstance()->Destroy();
            CollisionManager::DestroyInstance();
        }

        m_currentScene = m_nextScene;
        m_currentScene->m_app = this;
        m_currentScene->Load();
        m_currentScene->LoadLayers();
        m_nextScene = nullptr;
    }
}
void Application::SceneUpdate()
{
    if (m_nextScene != nullptr)
        return;

    if (m_currentScene)
    {
        m_currentScene->UpdateLayers(m_deltaTime);
        m_currentScene->Update(m_deltaTime);
        if (m_elapsedTimeStep >= m_timeStep)
        {
            m_currentScene->FixedLayers(m_timeStep);
            m_currentScene->FixedUpdate(m_timeStep);
            CollisionManager::GetInstance()->UpdateCollisions();
            m_elapsedTimeStep -= m_timeStep;
        }
    }
}
void Application::SceneFixedUpdate()
{
    while (m_elapsedTimeStep >= m_timeStep)
    {
        if (m_nextScene == nullptr && m_currentScene)
        {
            m_currentScene->FixedUpdate(m_timeStep);
            m_currentScene->FixedLayers(m_timeStep);
            CollisionManager::GetInstance()->UpdateCollisions();
        }
        FixedUpdate();
        FixedLayers();
        m_elapsedTimeStep -= m_timeStep;
    }
}
void Application::SceneRender()
{
    if (m_nextScene == nullptr && m_currentScene)
    {
        m_currentScene->m_camera.UpdateShader();
        m_currentScene->RenderLayers(m_deltaTime);
        m_currentScene->Render(m_deltaTime);
    }
}
void Application::SceneUnLoad()
{
    if (m_nextScene)
    {
        m_nextScene->UnLoad();
        m_nextScene->ClearScene();
        delete m_nextScene;
        m_nextScene = nullptr;
    }
    if (m_currentScene)
    {
        m_currentScene->UnLoad();
        m_currentScene->ClearScene();
        delete m_currentScene;
        m_currentScene = nullptr;
    }
}

Layer* Application::AddLayer()
{
    m_layers.push_back(new Layer());
    m_layers.back()->SetSceneID(m_layers.size() - 1);
    m_layers.back()->m_app = this;
    return m_layers.back();
}
Layer * Application::GetLayer(int a_index)
{
    if (a_index < m_layers.size())
        return m_layers[a_index];
    return nullptr;
}
void Application::LoadLayers()
{
    m_camera.SetOrtho(0.0f, (float)GetGameWidth(), 0.0f, (float)GetGameHeight());
    for (int i = 0; i < m_layers.size(); i++)
    {
        m_layers[i]->Load();
    }
}
void Application::UpdateLayers()
{
    for (int i = 0; i < m_layers.size(); i++)
    {
        m_layers[i]->Update(m_deltaTime);
    }
}
void Application::FixedLayers()
{
    for (int i = 0; i < m_layers.size(); i++)
    {
        m_layers[i]->FixedUpdate(m_timeStep);
    }
}
void Application::RenderLayers()
{
    for (int i = 0; i < m_layers.size(); i++)
    {
        m_layers[i]->Render(m_camera, m_deltaTime);
    }
}