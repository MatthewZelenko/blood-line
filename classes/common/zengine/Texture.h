#pragma once

class android_app;

class Texture
{
public:
	Texture();
	~Texture();

	void Destroy();

	void Load(const char* a_path);
	void Bind(int a_location);
	unsigned int GetID() { return m_id; }
	int GetWidth() { return m_width; }
	int GetHeight() { return m_height; }
	float GetInverseWidth() { return m_inverseWidth; }
	float GetInverseHeight() { return m_inverseHeight; }

private:
	unsigned int m_id;
	int m_width, m_height, m_comp;
	float m_inverseWidth, m_inverseHeight;
};