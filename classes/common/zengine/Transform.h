#pragma once
#include <GLM/mat4x4.hpp>
#include "Component.h"

class Transform : public Component
{
public:
	Transform();
	~Transform();

	void Translate(const glm::vec2& a_position);
	void TranslateX(float a_x);
	void TranslateY(float a_y);
	void SetPosition(const glm::vec2& a_position);
	void SetPositionX(float a_x);
	void SetPositionY(float a_y);
	void SetSize(const glm::vec2& a_size);
	void SetRotation(float a_rotation);

	//TODO: THERE MIGHT BE A BETTER WAY
	glm::mat4 GetModel();
	glm::mat4 GetLocalModel();
	glm::vec2 GetPosition();
	glm::vec2 GetLocalPosition() { return m_position; }
	glm::vec2 GetSize();
	glm::vec2 GetLocalSize() { return m_size; }
	float GetRotation();
	float GetLocalRotation() { return m_rotation; }
	void CalculateModel();

private:
	friend class Sprite;

	glm::mat4 m_model;
	glm::vec2 m_position;
	glm::vec2 m_size;
	float m_rotation;
};