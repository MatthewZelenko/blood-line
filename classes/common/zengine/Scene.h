#pragma once
#include "Layer.h"
#include "ResourceManager.h"
#include "Layer.h"
#include "Camera.h"
#include "Application.h"

class Scene
{
public:
	Scene();
	virtual ~Scene();
	void ClearScene();

	virtual void Load();
	virtual void Update(float a_deltaTime);
	virtual void FixedUpdate(float a_fixedTime);
	virtual void Render(float a_deltaTime);
	virtual void UnLoad();

	void SetName(std::string a_name);
	std::string GetName() { return m_name; }

	Layer* AddLayer();
	Layer* GetLayer(int a_index);

	Camera m_camera;
protected:
	Application* m_app;

private:
	friend class Application;
	void LoadLayers();
	void UpdateLayers(float a_deltaTime);
	void FixedLayers(float a_fixedTime);
	void RenderLayers(float a_deltaTime);
	std::string m_name;
	std::vector<Layer*> m_layers;
};