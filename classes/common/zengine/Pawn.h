#pragma once
#include"zengine/BoxCollider.h"
#include "zengine/Transform.h"
#include "zengine/Sprite.h"
#include "zengine/Texture.h"
#include "zengine/Shader.h"

class Layer;
class Pawn
{
public:
	Pawn();
	virtual ~Pawn();

	void InitPawn(Shader* a_shader, Texture* a_texture, const glm::vec2& a_position = glm::vec2(), const glm::vec2& a_size = glm::vec2());

	virtual void Load();
	virtual void Update(float a_deltaTime);
	virtual void FixedUpdate(float a_fixedTime);
	virtual void Render();
	virtual void OnCollisionEnter(Pawn* a_pawn);
	virtual void OnCollisionStay(Pawn* a_pawn);
	virtual void OnCollisionExit(Pawn* a_pawn);
	virtual void OnTriggerEnter(Pawn* a_pawn);
	virtual void OnTriggerStay(Pawn* a_pawn);
	virtual void OnTriggerExit(Pawn* a_pawn);

	void SetStatic(bool a_value) { m_isStatic = a_value; }
	void SetLayer(Layer* a_layer, int a_index);
	void SetName(std::string a_name) { m_name = a_name; }

	bool IsStatic() { return m_isStatic; }
	Layer* GetLayer() { return m_layer; }
	std::string GetName() { return m_name; }

	Transform m_transform;
	Sprite m_sprite;
	BoxCollider m_collider;

protected:
	std::string m_name;

private:
	bool m_isStatic;
	int m_layerIndex;
	Layer* m_layer;
	int m_colliderIndex;
};