#pragma once
#include <GLM/mat4x4.hpp>
class Shader;
class Camera
{
public:
	Camera();
	~Camera();

	void AttachShader(Shader* a_shader);

	void SetOrtho(float a_left, float a_right, float a_bottom, float a_top);
	void SetView(const glm::vec2& a_position, const glm::vec2& a_zoom, float a_rotation);

	void MoveTowards(const glm::vec2& a_position, float a_multiplier);
	void SetPosition(const glm::vec2& a_position) { m_position = a_position; }
	void Translate(const glm::vec2& a_position);
	void ClampPosition(const glm::vec2& a_min, const glm::vec2& a_max);
	void ClampXPosition(float a_min, float a_max);

	glm::mat4 GetProjection() { return m_projection; }
	glm::vec2 GetPosition() { return m_position; }

	void UpdateShader();
	void UpdateView(float a_strength);

private:
	Shader* m_shader;
	bool m_isDirty;

	glm::vec2 m_position;
	glm::vec2 m_zoom;
	float m_rotation;
	glm::mat4 m_projection;
	glm::mat4 m_view;

	float m_currentStrength;
};