#pragma once
#include <GLM/glm.hpp>
#include "zengine/Texture.h"
#include "zengine/Shader.h"

class Renderer
{
public:
	static Renderer* GetInstance();
	static void DestroyInstance();

	void Render(Texture a_texture, Shader a_shader, const glm::mat4& a_model, const glm::mat4& a_textureMat, const glm::vec4& a_colour);

private:
	Renderer();
	~Renderer();
	void Init();
	void Destroy();
	static Renderer* m_instance;

	unsigned int m_vao, m_vbo;
	unsigned int m_texture;
	unsigned int m_shader;
};