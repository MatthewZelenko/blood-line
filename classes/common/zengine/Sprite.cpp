#include "zengine/Sprite.h"
#include "zengine/Pawn.h"
#include <GLM/gtc/matrix_transform.hpp>

Sprite::Sprite() :
	m_colour(glm::vec4(1.0f)),
	m_texture(nullptr),
	m_shader(nullptr),
	m_flipped(glm::ivec2(0)),
	m_source(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f)),
	m_currentAnimation(-1),
	m_loop(0),
	m_animationSourceIndex(0),
	m_framesChangeSize(false),
	m_framesChangeColliderSize(false)
{
}
Sprite::~Sprite()
{
}
void Sprite::Animate(float a_deltaTime)
{
	if (m_currentAnimation == -1)
		return;
	Animation& anim = *m_animations[m_currentAnimation];
	if ((anim.m_loops != -1 && m_loop >= anim.m_loops) || anim.m_frameDelay == 0.0f || anim.m_frames.size() == 0)
		return;
	m_animationElapsed += a_deltaTime;
	while (m_animationElapsed >= anim.m_frameDelay)
	{
		SetSource(anim.m_frames[m_animationSourceIndex].m_source);
		SetAnimationOffset(anim.m_frames[m_animationSourceIndex].m_offset);
		SetOrigin(anim.m_frames[m_animationSourceIndex].m_origin);
		CalculateMat();
		m_parent->m_transform.CalculateModel();
		m_animationSourceIndex++;
		if (m_animationSourceIndex >= anim.m_frames.size())
		{
			m_animationSourceIndex = 0;
			if(anim.m_loops != -1)
				m_loop++;
		}
		m_animationElapsed -= anim.m_frameDelay;
	}
}
void Sprite::AddAnimation(Animation* a_animation)
{
	m_animations.push_back(a_animation);
	if (m_currentAnimation == -1)
	{
		m_loop = 0;
		m_currentAnimation = 0;
		m_animationElapsed = 0.0f;
		SetSource(m_animations[0]->m_frames[0].m_source);
		SetAnimationOffset(m_animations[0]->m_frames[0].m_offset);
		SetOrigin(m_animations[0]->m_frames[0].m_origin);
		CalculateMat();
	}
}
void Sprite::ChangeAnimation(int a_index, bool a_reset)
{
	if (a_index < m_animations.size() && (m_currentAnimation != a_index || a_reset))
	{
		m_currentAnimation = a_index;
		m_animationSourceIndex = 0;
		m_loop = 0;
		m_animationElapsed = 0.0f;
		SetSource(m_animations[m_currentAnimation]->m_frames[0].m_source);
		SetAnimationOffset(m_animations[m_currentAnimation]->m_frames[0].m_offset);
		SetOrigin(m_animations[m_currentAnimation]->m_frames[0].m_origin);
	}
	CalculateMat();
}
void Sprite::SetSource(const glm::vec4 & a_source)
{
	if (m_framesChangeSize)
	{
		m_parent->m_transform.SetSize(glm::vec2(a_source.z, a_source.w));
		m_parent->m_transform.CalculateModel();
	}
	if (m_framesChangeColliderSize)
	{
		m_parent->m_collider.SetSize(glm::vec2(a_source.z, a_source.w));
	}
	m_source = a_source;
	m_source.x *= m_texture->GetInverseWidth();
	m_source.y *= m_texture->GetInverseHeight();
	m_source.z *= m_texture->GetInverseWidth();
	m_source.w *= m_texture->GetInverseHeight();
}
void Sprite::SetFixedSource(const glm::vec4 & a_source)
{
	m_source = a_source;
}
void Sprite::SetFlippedX(bool a_value)
{
	m_flipped.x = a_value;
}

void Sprite::SetAnimationOffset(const glm::vec2 & a_offset)
{
	m_animationOffset = a_offset;
	m_parent->m_transform.CalculateModel();
}

void Sprite::SetOrigin(const glm::vec2 & a_origin)
{
	m_origin = a_origin;
	m_parent->m_transform.CalculateModel();
}

int Sprite::FinishedAnimation()
{
	if (m_animations[m_currentAnimation]->m_loops == -1)
		return -1;
	else if (m_loop == m_animations[m_currentAnimation]->m_loops)
		return 1;
	else
		return 0;
}

void Sprite::CalculateMat()
{
	m_textureMat = glm::mat4();
	m_textureMat = glm::translate(m_textureMat, glm::vec3(m_source.x, m_source.y, 0.0f));
	m_textureMat = glm::scale(m_textureMat, glm::vec3(m_source.z, m_source.w, 1.0f));
	m_textureMat = glm::scale(m_textureMat, glm::vec3(m_flipped.x ? -1.0f : 1.0f, m_flipped.y ? -1.0f : 1.0f, 1.0f));
	m_textureMat = glm::translate(m_textureMat, glm::vec3(-m_flipped, 0.0f));
}