#ifndef GAME_SHADER_H
#define GAME_SHADER_H
#include <vector>
#include <map>
#include <GLM/mat4x4.hpp>

class Shader
{
public:
    Shader();
    ~Shader();
    void Create();
	void Destroy();

    void AttachShaderFromString(std::string a_shader, unsigned int a_shaderType);
	void AttachShaderFromFile(std::string a_shader, unsigned int a_shaderType);
    void LinkProgram();
    void Use();
    static void UnUse();

	void BindLocation(char* a_name, int a_location);
    void SetMatrix4x4(char* a_location, const glm::mat4& a_mat);
	void SetVec4(char* a_location, const glm::vec4& a_vec);
	void SetIVec2(char * a_location, const glm::ivec2& a_vec);

    int GetID() { return m_id; }

private:
    int m_id;
    std::vector<int> m_shaders;
    std::map<char*, int> m_locations;
};
#endif //GAME_SHADER_H
