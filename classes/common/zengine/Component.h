#pragma once
class Pawn;

class Component
{
public:
	Component();
	~Component();

	void SetParent(Pawn* a_parent);
	Pawn* GetParent() { return m_parent; }

protected:
	Pawn* m_parent;
};