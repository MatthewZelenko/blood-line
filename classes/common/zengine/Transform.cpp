#include "Transform.h"
#include <GLM/gtc/matrix_transform.hpp>
#include "zengine/Pawn.h"
#include "Layer.h"

Transform::Transform() : m_rotation(0.0f),
m_size(glm::vec2(1.0f))
{
}
Transform::~Transform()
{
}

void Transform::SetSize(const glm::vec2& a_size)
{
	m_size = a_size;
	CalculateModel();
}
void Transform::SetRotation(float a_rotation)
{
	m_rotation = a_rotation;
	CalculateModel();
}

glm::mat4 Transform::GetModel()
{
	if (m_parent && m_parent->GetLayer())
		return m_parent->GetLayer()->m_transform.GetModel() * m_model;
	else
		return m_model;
}
glm::mat4 Transform::GetLocalModel()
{
	return m_model;
}
glm::vec2 Transform::GetPosition()
{
	if (m_parent && m_parent->GetLayer())
		return m_position + m_parent->GetLayer()->m_transform.GetPosition();
	else
		return m_position;
}
glm::vec2 Transform::GetSize()
{
	if (m_parent && m_parent->GetLayer())
		return m_size + m_parent->GetLayer()->m_transform.GetSize();
	else
		return m_size;
}
float Transform::GetRotation()
{
	if (m_parent && m_parent->GetLayer())
		return m_rotation + m_parent->GetLayer()->m_transform.GetRotation();
	else
		return m_rotation;
}
void Transform::Translate(const glm::vec2 & a_position)
{
	m_position += a_position;
	CalculateModel();
}
void Transform::TranslateX(float a_x)
{
	m_position.x += a_x;
	CalculateModel();
}
void Transform::TranslateY(float a_y)
{
	m_position.y += a_y;
	CalculateModel();
}
void Transform::SetPosition(const glm::vec2& a_position)
{
	m_position = a_position;
	CalculateModel();
}

void Transform::SetPositionX(float a_x)
{
	m_position.x = a_x;
	CalculateModel();
}

void Transform::SetPositionY(float a_y)
{
	m_position.y = a_y;
	CalculateModel();
}
void Transform::CalculateModel()
{
	m_model = glm::mat4();
	glm::vec2 pos = m_position;	

	if (m_parent)
	{
		float x = -m_parent->m_sprite.GetOrigin().x;
		if (m_parent->m_sprite.GetFlipped().x == 1)
			x = -(m_size.x + x);

		float y = -m_parent->m_sprite.GetOrigin().y;
		if (m_parent->m_sprite.GetFlipped().y == 1)
			y = -(m_size.y + y);

		m_model = glm::translate(m_model, glm::vec3(x, y, 0.0f));
	}
	m_model = glm::translate(m_model, glm::vec3(pos, 0.0f));
	m_model = glm::rotate(m_model, m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	m_model = glm::scale(m_model, glm::vec3(m_size.x, m_size.y, 1.0f));
}