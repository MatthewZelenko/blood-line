#include "Zengine/Renderer.h"
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#else
#include <GLEW/glew.h>
#endif

Renderer* Renderer::m_instance = nullptr;
Renderer::Renderer() :
m_vao(0),
m_vbo(0),
m_texture(0),
m_shader(0)
{
}
Renderer::~Renderer()
{
}

Renderer * Renderer::GetInstance()
{
	if (!m_instance)
	{
		m_instance = new Renderer();
		m_instance->Init();
	}
	return m_instance;
}
void Renderer::DestroyInstance()
{
	if (m_instance)
	{
		m_instance->Destroy();
		delete m_instance;
	}
	m_instance = nullptr;
}
void Renderer::Destroy()
{
	m_texture = 0;
	m_shader = 0;
	if (m_vbo)
		glDeleteBuffers(1, &m_vbo);
	if (m_vao)
		glDeleteVertexArrays(1, &m_vao);

	m_vbo = 0;
	m_vao = 0;
}

void Renderer::Init()
{
	//Square
	float verts[] = {
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f,		//TOP LEFT
		1.0f, 0.0f, 0.0f, 1.0f, 0.0f,			//TOP RIGHT
		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,			//BOTTOM RIGHT

		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,			//BOTTOM RIGHT
		0.0f, 1.0f, 0.0f, 0.0f, 1.0f,		//BOTTOM LEFT
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f		//TOP LEFT
	};
	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);

	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), &verts, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void*)(sizeof(float) * 3));
}
void Renderer::Render(Texture a_texture, Shader a_shader, const glm::mat4& a_model, const glm::mat4& a_textureMat, const glm::vec4& a_colour)
{
	unsigned int shaderID = a_shader.GetID();
	if (m_shader != shaderID)
	{
		a_shader.Use();
		m_shader = shaderID;
	}
	unsigned int textureID = a_texture.GetID();
	if (m_texture != textureID)
	{
		a_texture.Bind(0);
		m_texture = textureID;
	}
	a_shader.SetMatrix4x4("model", a_model);
	a_shader.SetMatrix4x4("textureMat", a_textureMat);
	a_shader.SetVec4("colour", a_colour);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}