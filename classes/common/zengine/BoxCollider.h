#pragma once
#include "Component.h"
#include <GLM/vec2.hpp>
#include <vector>

class BoxCollider : public Component
{
public:
	BoxCollider();
	~BoxCollider();

	void SetIsActive(bool a_isActive) { m_isActive = a_isActive; }
	void SetColliderId(int a_id);
	void SetIsTrigger(bool a_value);
	int GetID() { return m_id; }
	bool IsTrigger() { return m_isTrigger; }

	void SetPosition(const glm::vec2 a_pos);
	void SetSize(const glm::vec2& a_size);
	glm::vec2 GetSize() { return m_size; }
	glm::vec2 GetPosition() { return m_position; }
	bool IsActive() { return m_isActive; }

private:
	friend class CollisionManager;
	bool m_isTrigger, m_isActive;
	std::vector<bool> m_colliding;
	glm::vec2 m_position;
	glm::vec2 m_size;
	int m_id;
};