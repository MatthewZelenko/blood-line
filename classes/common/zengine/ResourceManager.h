#pragma once
#include <map>
#include "zengine/Texture.h"
#include "zengine/Shader.h"
#include "Animation.h"

class ResourceManager
{
public:
	static ResourceManager* GetInstance();
	static void DestroyInstance();
	void Destroy(bool a_all = false);

	Texture* CreateTexture(std::string a_textureName, bool a_keep = false);
	Shader* CreateShader(std::string a_shaderName, bool a_keep = false);
	Animation* CreateAnimation(std::string a_animationName, bool a_keep = false);

	Texture* GetTexture(std::string a_textureName);
	Shader* GetShader(std::string a_shaderName);
	Animation* GetAnimation(std::string a_animationName);

private:
	struct TextureStruct
	{
		Texture m_texture;
		bool m_keep = false;
	};
	struct ShaderStruct
	{
		Shader m_shader;
		bool m_keep = false;
	};
	struct AnimationStruct
	{
		Animation m_animation;
		bool m_keep = false;
	};
	ResourceManager();
	~ResourceManager();
	static ResourceManager* m_instance;

	std::map<std::string, TextureStruct> m_textures;
	std::map<std::string, ShaderStruct> m_shaders;
	std::map<std::string, AnimationStruct> m_animations;
};