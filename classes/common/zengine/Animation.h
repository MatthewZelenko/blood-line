#pragma once
#include <vector>
#include <GLM/vec4.hpp>
#include <GLM/vec2.hpp>

struct Frame {	
	glm::vec4 m_source;
	glm::vec2 m_offset;
	glm::vec2 m_origin;
};

struct Animation {

	void AddFrame(const glm::vec4& a_source, const glm::vec2& a_origin = glm::vec2(), const glm::vec2& a_offset = glm::vec2());
	
	std::vector<Frame> m_frames;
	float m_frameDelay = 0.0f;
	int m_loops = -1;
};