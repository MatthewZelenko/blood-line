#include "Camera.h"
#include <GLM/gtc/matrix_transform.hpp>
#include "Shader.h"


Camera::Camera() :
	m_zoom(1.0f),
	m_rotation(0.0f),
	m_isDirty(true),
	m_shader(nullptr),
	m_currentStrength(1.0f)
{
}
Camera::~Camera()
{
}

void Camera::AttachShader(Shader * a_shader)
{
	m_shader = a_shader;
}

void Camera::SetOrtho(float a_left, float a_right, float a_bottom, float a_top)
{
	m_projection = glm::ortho(a_left, a_right, a_bottom, a_top);
}
void Camera::UpdateShader()
{
	if (m_isDirty && m_shader)
	{
		m_shader->Use();
		m_shader->SetMatrix4x4("projection", m_projection);
		m_isDirty = false;
	}
}

void Camera::SetView(const glm::vec2& a_position, const glm::vec2& a_zoom, float a_rotation)
{
	m_position = a_position;
	m_zoom = a_zoom;
	m_rotation = a_rotation;
}
void Camera::MoveTowards(const glm::vec2 & a_position, float a_multiplier)
{
	glm::vec2 difference = a_position - m_position;
	if (glm::length(difference) <= 0.2f)
	{
		m_position = a_position;
	}
	else
	{
		difference *= a_multiplier;
		m_position += difference;
	}
}
void Camera::Translate(const glm::vec2 & a_position)
{
	m_position += a_position;
}
void Camera::ClampPosition(const glm::vec2 & a_min, const glm::vec2 & a_max)
{
	if (m_position.x < a_min.x)
		m_position.x = a_min.x;
	if (m_position.x > a_max.x)
		m_position.x = a_max.x;
	
	if (m_position.y < a_min.y)
		m_position.y = a_min.y;
	if (m_position.y > a_max.y)
		m_position.y = a_max.y;
}
void Camera::ClampXPosition(float a_min, float a_max)
{
	if (m_position.x < a_min)
		m_position.x = a_min;
	if (m_position.x > a_max)
		m_position.x = a_max;
}
void Camera::UpdateView(float a_strength)
{
	if (m_currentStrength == a_strength)
		return;
	m_currentStrength = a_strength;
	m_view = glm::rotate(glm::mat4(), m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));
	m_view = glm::scale(m_view, glm::vec3(m_zoom, 1.0f));
	m_view = glm::translate(m_view, glm::vec3(-m_position * m_currentStrength, 0.0f));
	if (m_shader)
	{
		m_shader->SetMatrix4x4("view", m_view);
	}
}