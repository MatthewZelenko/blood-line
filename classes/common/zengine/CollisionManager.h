#pragma once
#include <vector>
#include "Pawn.h"
class CollisionManager
{
public:
	static CollisionManager* GetInstance();
	static void DestroyInstance();

	void Register(Pawn* a_pawn);
	void UnRegister(int a_index);

	void UpdateCollisions();
	static void CheckCollision(int a_i, Pawn * a_pawn1, int a_j, Pawn * a_pawn2);
	static void ResolveCollision(Pawn * a_pawn1, const glm::vec2& a_min1, const glm::vec2& a_max1, Pawn * a_pawn2, const glm::vec2& a_min2, const glm::vec2& a_max2);

	void Render();

private:
	CollisionManager();
	~CollisionManager();
	static CollisionManager* m_instance;
	std::vector<int> m_freeIndices;
	std::vector<Pawn*>m_pawns;
};