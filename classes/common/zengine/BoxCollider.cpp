#include "BoxCollider.h"

BoxCollider::BoxCollider():
	m_position(glm::vec2(0.0f)),
	m_size(glm::vec2(0.0f)),
	m_isActive(true)
{
}
BoxCollider::~BoxCollider()
{
}
void BoxCollider::SetColliderId(int a_id)
{
	m_id = a_id;
}
void BoxCollider::SetIsTrigger(bool a_value)
{
	m_isTrigger = a_value;
}
void BoxCollider::SetPosition(const glm::vec2 a_pos)
{
	m_position = a_pos;
}
void BoxCollider::SetSize(const glm::vec2 & a_size)
{
	m_size = a_size;
}