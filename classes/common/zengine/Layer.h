#pragma once
#include <vector>
#include "Transform.h"
#include "Camera.h"

class Application;
class Pawn;
class Layer
{
public:
	Layer();
	~Layer();

	void AddPawn(Pawn* a_pawn);
	void RemovePawn(int index);

	void Load();
	void Update(float a_deltaTime);
	void FixedUpdate(float a_fixedTime);
	void Render(Camera& a_camera, float a_deltaTime);

	void SetStrength(float a_strength) { m_strength = a_strength; }
	void SetSceneID(int a_sceneID) { m_sceneID = a_sceneID; }
	void SetIsActive(bool a_isActive) { m_isActive = a_isActive; }

	bool GetActive() { return m_isActive; }
	float GetStrength() { return m_strength; }

	Transform m_transform;
	Application* m_app;
private:
	std::vector<int> m_freeIndices;
	std::vector<Pawn*> m_pawns;
	int m_sceneID;
	float m_strength;
	bool m_isActive;
};