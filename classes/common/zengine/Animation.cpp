#include "Animation.h"

void Animation::AddFrame(const glm::vec4 & a_source, const glm::vec2& a_origin, const glm::vec2 & a_offset)
{
	m_frames.push_back(Frame());
	m_frames.back().m_source = a_source;
	m_frames.back().m_offset = a_offset;
	m_frames.back().m_origin = a_origin;
}