#include "Layer.h"
#include "zengine/Pawn.h"

Layer::Layer() : m_strength(1.0f),
m_sceneID(-1),
m_isActive(true)
{
}
Layer::~Layer()
{
	m_freeIndices.clear();
	m_pawns.clear();
}

void Layer::AddPawn(Pawn * a_pawn)
{
	int id = 0;
	if (m_freeIndices.empty())
	{
		id = m_pawns.size();
		m_pawns.push_back(a_pawn);
	}
	else
	{
		id = m_freeIndices.back();
		m_freeIndices.pop_back();
		m_pawns[id] = a_pawn;
	}
	a_pawn->SetLayer(this, id);
}

void Layer::RemovePawn(int a_index)
{
	m_pawns[a_index]->SetLayer(nullptr, 0);
	m_pawns[a_index] = nullptr;
	m_freeIndices.push_back(a_index);
}

void Layer::Load()
{
	for (auto& iter : m_pawns)
	{
		iter->Load();
	}
}

void Layer::Update(float a_deltaTime)
{
	if (!m_isActive)
		return;
	for (auto& iter : m_pawns)
	{
		iter->Update(a_deltaTime);
	}
}

void Layer::FixedUpdate(float a_fixedTime)
{
	if (!m_isActive)
		return;
	for (auto& iter : m_pawns)
	{
		iter->FixedUpdate(a_fixedTime);
	}
}

void Layer::Render(Camera& a_camera, float a_deltaTime)
{
	if (!m_isActive)
		return;
	m_transform.SetPosition(-a_camera.GetPosition() * m_strength);
	for (auto& iter : m_pawns)
	{
		iter->m_sprite.Animate(a_deltaTime);
		Renderer::GetInstance()->Render(iter->m_sprite.GetTexture(), iter->m_sprite.GetShader(), m_transform.GetLocalModel() * iter->m_transform.GetLocalModel(), iter->m_sprite.GetTextureMat(), iter->m_sprite.GetColour());
		iter->Render();
	}
}
