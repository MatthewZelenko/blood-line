#include "Ladder.h"
#include "Player.h"

Ladder::Ladder()
{
	m_name = "Ladder";
	SetStatic(true);
	m_collider.SetIsTrigger(true);
}
Ladder::~Ladder()
{
}
void Ladder::Load()
{
	m_sprite.ChangeAnimation(0);
	m_collider.SetPosition(glm::vec2(3.0f, 0.0f));
	m_collider.SetSize(glm::vec2(7.0f, 104.0f));
}

void Ladder::OnTriggerEnter(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		m_sprite.ChangeAnimation(1);
	}
}
void Ladder::OnTriggerStay(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		m_sprite.ChangeAnimation(1);
	}
}
void Ladder::OnTriggerExit(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		m_sprite.ChangeAnimation(0);
	}
}