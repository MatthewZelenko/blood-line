#include "HidingSpot.h"
#include "Player.h"

HidingSpot::HidingSpot()
{
	m_name = "HidingSpot";
	SetStatic(true);
	m_collider.SetIsTrigger(true);
}
HidingSpot::~HidingSpot()
{

}
void HidingSpot::OnTriggerEnter(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		if (((Player*)a_pawn)->IsHiding())
		{
			m_sprite.ChangeAnimation(1);
		}
		else
		{
			m_sprite.ChangeAnimation(2);
		}
	}
}
void HidingSpot::OnTriggerStay(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		if (((Player*)a_pawn)->IsHiding())
		{
			m_sprite.ChangeAnimation(1);
		}
		else
		{
			m_sprite.ChangeAnimation(2);
		}
	}
}
void HidingSpot::OnTriggerExit(Pawn * a_pawn)
{
	if (a_pawn->GetName() == "Player")
	{
		m_sprite.ChangeAnimation(0);
	}
}
void HidingSpot::Load()
{
	m_sprite.ChangeAnimation(0);
	m_collider.SetSize(glm::vec2(24.0f, 60.0f));
	m_collider.SetPosition(glm::vec2(4.0f, 0.0f));
}