#pragma once
#include "zengine/Pawn.h"

class PatrolGuard;
class HidingSpot;
class Ladder;
class Application;
class Player : public Pawn
{
public:
	Player();
	~Player();

	void Init(float a_sneakSpeed, float a_walkSpeed, float a_climbSpeed);
	void Update(float a_deltaTime);
	void OnCollisionEnter(Pawn* a_pawn);
	void OnCollisionStay(Pawn* a_pawn);
	void OnCollisionExit(Pawn* a_pawn);
	void OnTriggerEnter(Pawn* a_pawn);
	void OnTriggerStay(Pawn* a_pawn);
	void OnTriggerExit(Pawn* a_pawn);

	glm::vec2 GetCameraPosition() { return m_cameraPosition; }
	bool HasKey() { return m_hasKey; }
	bool IsHiding() { return m_isHiding; }
	bool IsAmbushing() { return m_isAmbushing; }
	int HasWon() { return m_hasWon; }
	void SetHasWon(int a_value);

	void ClampPosition(float a_min, const float a_max);

private:
	friend class HidingSpot;

	bool Stab();
	void Move(float a_deltaTime);
	void Look(float a_deltaTime);

	void Hide(HidingSpot* a_pawn);
	void Kill(PatrolGuard* a_guard);
	void Climb(Ladder* a_ladder);

	float m_acceleration;
	float m_sneakSpeed, m_walkSpeed, m_climbSpeed;
	int m_isClimbing;
	bool m_isGrounded;
	bool m_isHiding;
	int m_hasWon;
	bool m_hasKey;
	bool m_isAmbushing;
	int m_direction;
	glm::vec2 m_cameraPosition;
	float m_idleElapsed;
};