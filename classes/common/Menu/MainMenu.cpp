#include "MainMenu.h"
#include <GLM/gtc/matrix_transform.hpp>
#ifdef __ANDROID__
#include <GLES3/gl3.h>
#else
#include <GLEW/glew.h>
#endif
#include "zengine/Input.h"

MainMenu::MainMenu()
{

}
MainMenu::~MainMenu()
{
}

void MainMenu::Load()
{
	//SHADER
	Shader* shader = ResourceManager::GetInstance()->GetShader("normal");
	shader->Use();
	m_camera.AttachShader(shader);

	float centerX = m_app->GetGameWidth() * 0.5f;
	Layer* layer = AddLayer();

	Texture* texture = ResourceManager::GetInstance()->CreateTexture("Bg");
	texture->Load("textures/Menu/Bg.png");
	m_bg.InitPawn(shader, texture);
	layer->AddPawn(&m_bg);


	texture = ResourceManager::GetInstance()->CreateTexture("Buttons");
	texture->Load("textures/Menu/Menu Buttons.png");

	//PLAY
	m_play.InitPawn(shader, texture, glm::vec2(centerX - (36.0f * 0.5f), (m_app->GetGameHeight() * 0.6f) - 40.0f), glm::vec2(36.0f, 17.0f));
	m_play.m_major = 1;
	m_play.m_minor = 0;

	Animation* anim = ResourceManager::GetInstance()->CreateAnimation("Play_Up");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(0.0f, 0.0f, 36.0f, 17.0f));
	m_play.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("Play_Down");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(36.0f, 0.0f, 36.0f, 17.0f));
	m_play.m_sprite.AddAnimation(anim);

	layer->AddPawn(&m_play);

	//EXIT
	m_exit.InitPawn(shader, texture, glm::vec2(centerX - (36.0f * 0.5f), (m_app->GetGameHeight() * 0.6f) - 60.0f), glm::vec2(36.0f, 17.0f));
	m_exit.m_major = 0;
	m_exit.m_minor = 0;

	anim = ResourceManager::GetInstance()->CreateAnimation("Exit_Up");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(0.0f, 17.0f, 36.0f, 17.0f));
	m_exit.m_sprite.AddAnimation(anim);

	anim = ResourceManager::GetInstance()->CreateAnimation("Exit_Down");
	anim->m_loops = 1;
	anim->AddFrame(glm::vec4(36.0f, 17.0f, 36.0f, 17.0f));
	m_exit.m_sprite.AddAnimation(anim);

	layer->AddPawn(&m_exit);

	texture = ResourceManager::GetInstance()->CreateTexture("Title");
	texture->Load("textures/Menu/Title.png");
	m_title.InitPawn(shader, texture, glm::vec2(centerX - (texture->GetWidth() * 0.5f), (m_app->GetGameHeight() * 0.6f)));
	layer->AddPawn(&m_title);
}
void MainMenu::Update(float a_deltaTime)
{
}
void MainMenu::Render(float a_deltaTime)
{
}
void MainMenu::UnLoad()
{
}