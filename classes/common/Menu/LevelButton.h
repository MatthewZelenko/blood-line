#pragma once
#include "zengine/Pawn.h"

class LevelButton : public Pawn
{
public:
	LevelButton();
	~LevelButton();

	void Load();
	void Update(float a_deltaTime);

	bool PointInside(const glm::vec2& a_pos);

	int m_major, m_minor;
	bool m_clicked;

protected:
	glm::vec2 m_min;
	glm::vec2 m_max;
};