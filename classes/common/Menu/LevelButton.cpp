#include "LevelButton.h"
#include "zengine/Layer.h"
#include "Game.h"
#include "zengine/Input.h"

LevelButton::LevelButton()
{
}
LevelButton::~LevelButton()
{
}

void LevelButton::Load()
{
	m_min = m_transform.GetLocalPosition();
	m_max = m_min + m_transform.GetLocalSize();
}

void LevelButton::Update(float a_deltaTime)
{
	glm::vec2 clickPosition;
	int success = (Input::GetInstance()->Click(clickPosition) ? 1 : 0) | (Input::GetInstance()->Release(clickPosition) ? -1 : 0);

	if (success == 1)
	{
		if (PointInside(clickPosition))
		{
			m_clicked = true;
			m_sprite.ChangeAnimation(1);
		}
		else
		{
			m_clicked = false;
			m_sprite.ChangeAnimation(0);
		}
	}
	else if (success == -1)
	{
		m_sprite.ChangeAnimation(0);
		if (m_clicked && PointInside(clickPosition))
		{
			((Game*)(GetLayer()->m_app))->LoadLevel(m_major, m_minor);
		}
	}
}

bool LevelButton::PointInside(const glm::vec2 & a_pos)
{
	if (a_pos.x > m_min.x && a_pos.x < m_max.x &&
		a_pos.y > m_min.y && a_pos.y < m_max.y)
	{
		return true;
	}
	return false;
}