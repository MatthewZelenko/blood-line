#pragma once
#include "zengine/Scene.h"
#include "LevelButton.h"

class Application;
class MainMenu : public Scene
{
public:
	MainMenu();
	~MainMenu();

	void Load();
	void Update(float a_deltaTime);
	void Render(float a_deltaTime);
	void UnLoad();

	Pawn m_bg, m_title;
	LevelButton m_play;
	LevelButton m_exit;

	glm::mat4 m_projection;
};