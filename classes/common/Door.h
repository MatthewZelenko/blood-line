#pragma once
#include "zengine/Pawn.h"

class Application;
class Door : public Pawn
{
public:
	Door();
	~Door();

	void Init(int a_majorLevel, int a_minorLevel);

	void OnTriggerEnter(Pawn* a_pawn);
	void OnTriggerStay(Pawn* a_pawn);

private:
	int m_majorLevel, m_minorLevel;
};