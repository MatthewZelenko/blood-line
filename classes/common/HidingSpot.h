#pragma once
#include "zengine/Pawn.h"

class HidingSpot : public Pawn
{
public:
	HidingSpot();
	virtual ~HidingSpot();

	void OnTriggerEnter(Pawn* a_pawn);
	void OnTriggerStay(Pawn* a_pawn);
	void OnTriggerExit(Pawn* a_pawn);

	void Load();
private:
};