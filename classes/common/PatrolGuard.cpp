#include "PatrolGuard.h"
#include "L1/L1Selector.h"
#include "zengine/CollisionManager.h"
#include "Game.h"

PatrolGuard::PatrolGuard() : m_state(PATROL),
m_elapsed(0.0f),
m_direction(1)
{
	m_name = "PatrolGuard";
}
PatrolGuard::~PatrolGuard()
{

}

void PatrolGuard::Init(float a_speed, Player * a_player, float a_startPosition, float a_endPosition)
{
	m_player = a_player;
	m_startPosition = a_startPosition;
	m_transform.SetPosition(glm::vec2(m_startPosition, 13.0f));
	m_endPosition = a_endPosition;
	m_speed = a_speed;
	m_state = PATROL;
	m_sprite.ChangeAnimation(1);
	m_elapsed = 0.0f;
	m_direction = 1;
	m_isDead = false;
	m_collider.SetPosition(glm::vec2(-m_transform.GetSize().x * 0.5f, 0.0f));
}

void PatrolGuard::Update(float a_deltaTime)
{
	if (m_isDead)
		return;
	switch (m_state)
	{
	case IDLE:
	{
		m_elapsed += a_deltaTime;
		if (m_elapsed >= 3.0f)
		{
			m_state = PATROL;
			m_elapsed = 0.0f;
			m_direction = !m_direction;
			m_sprite.SetFlippedX(!m_sprite.GetFlipped().x);
			m_sprite.ChangeAnimation(1);
		}
	}
	break;
	case PATROL:
	{
		if (m_direction)
		{
			float dir = m_endPosition - m_transform.GetLocalPosition().x;
			if (glm::abs(dir) <= 1.0f)
			{
				m_transform.SetPosition(glm::vec2(m_endPosition, 13.0f));
				m_state = IDLE;
				m_sprite.ChangeAnimation(0);
			}
			else
			{
				m_transform.Translate(glm::vec2(glm::sign(dir) * m_speed * a_deltaTime, 0.0f));
			}
		}
		else
		{
			float dir = m_startPosition - m_transform.GetLocalPosition().x;
			if (glm::abs(dir) <= 1.0f)
			{
				m_transform.SetPosition(glm::vec2(m_startPosition, 13.0f));
				m_state = IDLE;
				m_sprite.ChangeAnimation(0);
			}
			else
			{
				m_transform.Translate(glm::vec2(glm::sign(dir) * m_speed * a_deltaTime, 0.0f));
			}
		}
	}
	break;
	case CHASE:
	{
		float dir = m_player->m_transform.GetLocalPosition().x - m_transform.GetLocalPosition().x;
		if (glm::abs(dir) >= 20.0f)
		{
			m_transform.Translate(glm::vec2(glm::sign(dir) * m_speed * 2.0f * a_deltaTime, 0.0f));
		}
		else
		{
			m_sprite.ChangeAnimation(0);
			return;
		}
	}
	break;
	default:
		break;
	}

	float distance = (m_player->m_transform.GetLocalPosition().x - m_transform.GetLocalPosition().x);
	if (m_direction)
	{
		if (!m_player->IsHiding() && !m_player->IsAmbushing() && distance <= 64.0f && distance >= -4.0f)
		{
			m_state = CHASE;
			m_sprite.ChangeAnimation(1);
			m_player->SetHasWon(-1);
			((Game*)GetLayer()->m_app)->Fade(true, 1.0f);
		}
	}
	else
	{
		if (!m_player->IsHiding() && !m_player->IsAmbushing() && distance >= -64.0f && distance <= 4.0f)
		{
			m_state = CHASE;
			m_sprite.ChangeAnimation(1);
			m_player->SetHasWon(-1);
			((Game*)GetLayer()->m_app)->Fade(true, 1.0f);
		}
	}
}

void PatrolGuard::GiveKey()
{
	m_hasKey = true;
}

bool PatrolGuard::Kill()
{
	m_isDead = true;
	CollisionManager::GetInstance()->UnRegister(m_collider.GetID());
	m_sprite.ChangeAnimation(2);
	return m_hasKey;
}

float PatrolGuard::GetBehind(bool& a_direction)
{
	a_direction = m_direction;
	if(!m_direction)
		return m_transform.GetLocalPosition().x + m_transform.GetLocalSize().x - 3.0f;
	else
		return m_transform.GetLocalPosition().x - m_transform.GetLocalSize().x + 3.0f;
}
