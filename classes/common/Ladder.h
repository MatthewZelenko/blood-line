#pragma once
#include "zengine/Pawn.h"
class Ladder : public Pawn
{
public:
	Ladder();
	~Ladder();

	void Load();

	void OnTriggerEnter(Pawn* a_pawn);
	void OnTriggerStay(Pawn* a_pawn);
	void OnTriggerExit(Pawn* a_pawn);
};