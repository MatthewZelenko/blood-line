in vec3 position;
in vec2 texCoords;

out vec2 vTexCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 textureMat;

void main()
{
	gl_Position = projection * model * vec4(position, 1.0f);
	vTexCoords = vec2(textureMat * vec4(texCoords.x, 1.0f - texCoords.y, 0.0f, 1.0f));
}