in vec2 vTexCoords;

uniform sampler2D texture_diffuse0;
uniform vec4 colour;

out vec4 fColour;

void main()
{
	fColour = texture(texture_diffuse0, vTexCoords) * colour;
	if(fColour.a <= 0.01f)
		discard;
}